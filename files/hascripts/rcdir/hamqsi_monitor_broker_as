#!/bin/bash
# Module:
#   hamqsi_monitor_broker_as
#
# Args:
#   BROKER = name of broker in AppServer
#   QM     = name of queue manager in AppServer
#   IIBUSER = userid under which queue manager and broker run
# 
# Description:
#   This is the application monitor script used with HACMP/ES. It 
#   needs to be invoked by a parameter-less wrapper script because
#   HACMP does not allow parameters to be passed to application
#   monitor scripts.
#  
#   This hamqsi_monitor_broker_as script is run as root, and uses 
#   su as needed to monitor the 3 components of the application server.
#  
#   This script is tolerant of a queue manager that is still in
#   startup. If the queue manager is still starting this application 
#   monitor script will exit with 0 - which indicates
#   to HA Cluster that there's nothing wrong. This is to allow for
#   startup time for the queue manager which might exceed the 
#   Stabilisation Interval set for the Application Monitor in HA Cluster.
#    
#  
# Exit codes:
#   0  => Broker & QM are all running OK or starting
#   >0 => One or more components are not responding.
#

# Check running as root
if [ `id -u`  -ne 0 ]
then
  echo "Must be running as root"
  exit 1
fi

BROKER=$1
QM=$2
IIBUSER=$3
# Check the parameters

if [ -z "$BROKER" ]
then
  echo "hamqsi_monitor_broker_as: ERROR! No IIB Node name supplied"
  exit 1
fi

if [ -z "$QM" ]
then
  echo "hamqsi_monitor_broker_as: ERROR! No queue manager name supplied"
  exit 1
fi

if [ -z "$IIBUSER" ]
then
  echo "hamqsi_monitor_broker_as: ERROR! No mquser supplied"
  exit 1
fi

# Use a state variable to reflect the state of components as they
# are tested. Valid values are "stopped", "starting" and "started"
# Initialise it to "stopped" for safety. 
STATE="stopped"

# ------------------------------------------------------------------
# Check that the queue manager is running or starting.
#
su - $IIBUSER -c "echo 'ping qmgr' | runmqsc ${QM}" > /dev/null 2>&1
pingresult=$?
# pingresult will be 0 on success; non-zero on error (man runmqsc)
if [ $pingresult -eq 0 ]
then 
  # ping succeeded
  echo "hamqsi_monitor_broker_as: Queue Manager ${QM} is responsive"
  STATE="started"
else 
  # ping failed
  # Don't condemn the QM immediately, it might be in startup.
  # The following regexp includes a space and a tab, so use tab-friendly
  # editors.
  srchstr=" $QM[  ]*$"
  cnt=`ps -ef | grep strmqm | grep "$srchstr" | grep -v grep \
                | awk '{print $2}' | wc -l`
if [ $cnt -gt 0 ]
  then
    # It appears that QM is still starting up, tolerate
    echo "hamqsi_monitor_broker_as: Queue Manager ${QM} is starting"
    STATE="starting"
  else
    # There is no sign of QM start process
    echo "hamqsi_monitor_broker_as: Queue Manager ${QM} is not responsive"
    STATE="stopped"
  fi
fi


# Decide whether to continue or to exit
case $STATE in
  stopped)
    echo "hamqsi_monitor_broker_as: Queue manager ($QM) is not running correctly"
    exit 1  
    ;;
  starting)
    echo "hamqsi_monitor_broker_as: Queue manager ($QM) is starting"
    echo "hamqsi_monitor_broker_as: WARNING - Stabilisation Interval might be too short"
    echo "hamqsi_monitor_broker_as: WARNING - No test of IIB Node $BROKER will be conducted"
    exit 0  
    ;;
  started)
    echo "hamqsi_monitor_broker_as: Queue manager ($QM) is running"
    ;;
esac

# ------------------------------------------------------------------
# Check the IIB Node is running 
# 
# Re-initialise STATE for safety
STATE="stopped"
#
# The broker runs as a process called bipservice which is responsible
# for starting and re-starting the admin agent process (bipbroker). 
# The bipbroker is responsible for starting any DataFlowEngines. The 
# bipbroker starts the DataFlowEngines using the wrapper script 
# startDataFlowEngine. If no integration servers have been assigned to 
# the broker there will be no DataFlowEngine processes. There should 
# always be a bipservice and bipbroker process pair. This monitor 
# script only tests for bipservice, because bipservice should restart
# bipbroker if necessary - the monitor script should not attempt to 
# restart bipbroker and it might be premature to report an absence 
# of a bipbroker as a failure.
#
cnt=`ps -ef | grep "bipservice $BROKER" | grep -v grep | wc -l`
if [ $cnt -eq 0 ]
then 
  echo "hamqsi_monitor_broker_as: IIB Node $BROKER is not running"
  STATE="stopped"
else
  echo "hamqsi_monitor_broker_as: IIB Node $BROKER is running" 
  STATE="started"
fi

# Decide how to exit
case $STATE in
  stopped)
    echo "hamqsi_monitor_broker_as: Node ($BROKER) is not running correctly"
    exit 1  
    ;;
  started)
    echo "hamqsi_monitor_broker_as: Node ($BROKER) is running"
    exit 0 
   ;;
esac 
