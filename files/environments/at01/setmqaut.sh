
QMNAME=$1
GRPNAME=$2
QMGRPERMSTR=$3
CHLRPERMSTR=$4
QRPERMSTR=$5

setmqaut -m ${QMNAME} -g ${GRPNAME} -t qmgr ${QMGRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t channel -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t listener -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t authinfo -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t clntconn -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t comminfo -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t namelist -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t process -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t service -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t topic -n "**" ${CHLRPERMSTR}
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n "**" -all +inq +dsp +browse
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.** -all +inq +dsp +browse
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.MQEXPLORER.REPLY.MODEL -all +inq +dsp +get
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.DEFAULT.MODEL.QUEUE -all +inq +dsp +browse +get
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.BROKER.DC.AUTH -all +dsp +inq +put
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.BROKER.DEPLOY.QUEUE -all +dsp +inq +browse +put
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.BROKER.DEPLOY.REPLY -all +inq +dsp +browse +get +put
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.ADMIN.COMMAND.QUEUE -all +inq +dsp +put +inq
setmqaut -m ${QMNAME} -g ${GRPNAME} -t topic -n SYSTEM.BROKER.MB.TOPIC -all +sub +pub
#setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.BROKER.AUTH.{EG} -all +dsp +inq

setmqaut -m ${QMNAME} -g ${GRPNAME} -t qmgr -all +connect +inq +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t channel -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t listener -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t authinfo -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t clntconn -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t comminfo -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t namelist -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t process -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t service -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t topic -n "**" -all +dsp
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n "**" -all +inq +dsp +browse
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.** -all +inq +dsp +browse
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.MQEXPLORER.REPLY.MODEL -all +inq +dsp +get
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.DEFAULT.MODEL.QUEUE -all +inq +dsp +browse +get
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.BROKER.DC.AUTH -all +dsp +inq +put
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.BROKER.DEPLOY.QUEUE -all +dsp +inq +browse +put
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.BROKER.DEPLOY.REPLY -all +inq +dsp +browse +get +put
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.ADMIN.COMMAND.QUEUE -all +inq +dsp +put +inq
setmqaut -m ${QMNAME} -g ${GRPNAME} -t topic -n SYSTEM.BROKER.MB.TOPIC -all +sub +pub
setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.BROKER.AUTH.{EG} -all +dsp +inq
