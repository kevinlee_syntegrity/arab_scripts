
QMNAME=$1
GRPNAME=$2

MQUSR="mqm"

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su $MQUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t qmgr -all +connect +crt +dlt +inq +dsp +set +setall"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t channel -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t channel -n SYSTEM.* -all +ctrl +ctrlx +dsp"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t listener -n \"**\" -all +ctrl +dsp"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t authinfo -n \"**\" -all +dsp"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t clntconn -n \"**\" -all +dsp"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t comminfo -n \"**\" -all +dsp"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t namelist -n \"**\" -all +dsp"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t process -n \"**\" -all +dsp"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t service -n \"**\" -all +ctrl +dsp"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t topic -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.** -all +inq +dsp +get +put +browse +clr"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t topic -n SYSTEM.BROKER.MB.TOPIC +all"
