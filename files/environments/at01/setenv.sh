###########################################
# 		EUT01 environment
###########################################

#Instance01 properties
###########################################
export NODE_HOST=iib-03at01.rms.nsw.gov.au
export NODE_IP=10.50.24.54

#MQ properties
export MQ_STD_NODE_QMGRNAME=IBQMAT01
export MQ_STD_NODE_QMGRCOM="QMgr for AT environment"
export MQ_STD_NODE_QMGRPORT=1414
export MQ_STD_NODE_QMGRLP=10
export MQ_STD_NODE_QMGRLS=5
export MQ_STD_NODE_QMGRLF=32768
export MQ_STD_NODE_QMGRHOST=$NODE_HOST
#Remeber to change
export MQ_STD_NODE_WORKPATH=/var/mqm
export MQ_STD_GRPS="mqm mqsupp"

#IIB Properties
export IB_STD_NODE_NODENAME=IBNDAT01
export IB_STD_NODE_SHARED_PATH=/var/mqsi
export IB_STD_NODE_USR_EXITLILPATH=/usr/src/rms-iib
export IB_STD_NODE_WEB_PORT=4414
export IB_STD_NODE_GC_PORT=2800
export IB_STD_NODE_MQTT_PORT=11831

# IIB SSL details
export IB_STD_NODE_SSLKEYFILE=rmsdomain.np.jks #Provide keystore file name
export IB_STD_NODE_SSLKEYPASS=password #Provide password
export IB_STD_NODE_SSLTRUSTFILE=rmscerts.np.jks #Provide keystore file name
export IB_STD_NODE_SSLTRUSTPASS=password #Provide password

export IB_STD_WEB_IIBADM_PWD=iibadm@00
export IB_STD_WEB_IIBUSR_PWD=iibuser@00

#Configurable service specifics
export IB_STD_OLDAPOBJ=OPENLDAP_01
export IB_STD_OLDAPAUTCONFIG=ldap://192.168.189.50:389/dc=ibm,dc=com
export IB_STD_OLDAPAUZCONFIG=ldap://ldap.acme.com:389/cn=root,ou=acmegroups,o=acme.com

export IB_STD_EDIROBJ=EDIRECTORY_01
export IB_STD_EDIRAUTCONFIG=ldap://192.168.189.50:389/dc=ibm,dc=com
export IB_STD_EDIRAUZCONFIG=ldap://ldap.acme.com:389/cn=root,ou=acmegroups,o=acme.com

export IB_STD_EMAILOBJ=EMAILSERVER_01
export IB_STD_EMAILSECID=emailserverid
export IB_STD_EMAILSRVSTR=pop3://192.168.20.202:110

export IB_STD_ODBC_DSN=ORACLEODBC_01
export IB_STD_ODBC_UID=STAUG15CP
export IB_STD_ODBC_PWD=phu5rabe

export IB_JDBC_ORA_SRVNAME=intdbs14.rta.nsw.gov.au
export IB_JDBC_ORA_UID=STAUG15CP
export IB_JDBC_ORA_PWD=phu5rabe
export IB_JDBC_ORA_OBJ=DB_EMPLOYEE_01
export IB_JDBC_ORA_SID=STAUG15
export IB_JDBC_ORA_MAXCONN=3
export IB_JDBC_ORA_PORT=1521
export IB_JDBC_ORA_DBNAME=employees
export IB_JDBC_ORA_DESC="RMS Employee JDBC Service"

echo "Environment variables are set."
