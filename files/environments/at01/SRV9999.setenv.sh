###########################################
# 			EUT environment
###########################################

#SRV9999 properties
###########################################
#IIB Properties
export IB_SRV_NAME=SRV9999
export IB_SRV_HTTP_PORT=7801
export IB_SRV_HTTPS_PORT=7841
export IB_SRV_JVM_PORT=8878
export IB_SRV_USR_EXITLILPATH=/usr/src/rms-iib

# IIB SSL details
export IB_SRV_SSLKEYFILE=rmsdomain.np.jks #Provide keystore file name
export IB_SRV_SSLKEYPASS=password #Provide password
export IB_SRV_SSLTRUSTFILE=rmscerts.np.jks #Provide keystore file name
export IB_SRV_SSLTRUSTPASS=password #Provide keystore file name

echo "Environment variables are set."
