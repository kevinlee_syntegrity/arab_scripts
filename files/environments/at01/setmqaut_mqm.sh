
QMNAME=$1
GRPNAME=$2

MQUSR="mqm"

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su $MQUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t qmgr +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t channel -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t listener -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t authinfo -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t clntconn -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t comminfo -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t namelist -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t process -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t service -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t topic -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n \"**\" +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t queue -n SYSTEM.** +all"
doExec "setmqaut -m ${QMNAME} -g ${GRPNAME} -t topic -n SYSTEM.BROKER.MB.TOPIC +all"

