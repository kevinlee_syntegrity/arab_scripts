#!/bin/sh
#
# Script to verify and print the HTTP/HTTPS WebUI details to console.
#

# Assign Values 

NODE="ABNDPR01"


echo "========================================"
echo "${NODE}_Verify.sh - Started"
echo "========================================"

# Confirm the Web User Interface Values are set properly

echo "========================================"
echo "WebUI Connector Details"
echo "========================================"

mqsireportproperties ${NODE} -b webadmin -o HTTPConnector -a
mqsireportproperties ${NODE} -b webadmin -o HTTPSConnector -a


echo "========================================="
echo "WebUI Server Details"
echo "========================================="

mqsireportproperties  ${NODE} -b webadmin -o server -a 

echo "========================================"
echo "${NODE}_Verify.sh - ended."
echo "========================================"
