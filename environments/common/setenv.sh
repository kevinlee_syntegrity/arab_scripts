###########################################
#Generic properties
###########################################
echo "set generic environment variables"

export ROOT_INSTALL_DIR=/tmp/iib/install
export SW_DIR=${ROOT_INSTALL_DIR}/softs
export SH_DIR=${ROOT_INSTALL_DIR}/IIBv10scripts
#For Prod and LT
#export USR_HOME=/home
#For other environments
export USR_HOME=/export/home

export ORA_ROOT_INSTALL_DIR=/opt/oracle
export IBM_ROOT_INSTALL_DIR=/opt/ibm
export IB_ROOT_WORK_DIR=/var/mqsi
export IB_INSTANCE_ADM=iibadmin
export IB_INSTANCE_UID=5002
export IB_INSTANCE_GRP=mqbrkrs
export IB_INSTANCE_GID=5002
export IB_INSTANCE_UMASK=0002
export MQ_INSTANCE_USR=mqm
export MQ_INSTANCE_UID=5001
export MQ_INSTANCE_GRP=mqm
export MQ_INSTANCE_GID=5001
export MQ_INSTANCE_UMASK=NA
export MQ_SUPPORT_GRP=mqsupp
export MQ_SUPPORT_GID=5003
export MQ_SW_DIR=$SW_DIR
export MQ_SW_PKG=IBM_MQ_9.0.0.0_LINUX_X86-64.tar.gz
export MQ_SW_FPPKG=9.0.0-IBM-MQ-LinuxX64-FP0002.tar.gz
export RPM_OPT1=-ivh
export RPM_OPT2=--prefix
export IB_SW_DIR=$SW_DIR
export IB_SW_PKG=10.0.0-IIB-LINUXX64-FP0010.tar.gz

export MQ_TMP_DIR=/var/tmp
export MQ_INST1_NAME=inst1
export MQ_INST2_NAME=inst2
export MQ_INSTALL_ROOT_DIR=${IBM_ROOT_INSTALL_DIR}/mqm
export MQ_INSTALL_INST1_DIR=${MQ_INSTALL_ROOT_DIR}/${MQ_INST1_NAME}
export MQ_INSTALL_INST2_DIR=${MQ_INSTALL_ROOT_DIR}/${MQ_INST2_NAME}
export IB_SYM_LINK_INSTALDIR=${IBM_ROOT_INSTALL_DIR}/iib

#SSL related
export SSL_DIR=${IB_ROOT_WORK_DIR}/ssl

export IB_LOG_LOC=/var/iib/logs
export IB_ACTIVITY_LOGLOC=$IB_LOG_LOC
export IB_ODBC_DIR=${IB_ROOT_WORK_DIR}/odbc
export IB_JDBC_ORA_DIR=${ORA_ROOT_INSTALL_DIR}/jdbc/lib
export IB_JDBC_ORA_VER=11.2
export IB_CONNECTOR_DIR=${IB_ROOT_WORK_DIR}/connectors

export IB_STD_NODE_INSTALLDIR=${IBM_ROOT_INSTALL_DIR}/mqsi
#export IB_STD_NODE_SHARED_WORKPATH=/ha/iib
export IB_STD_NODE_MODE=standard
export IB_STD_NODE_CONFIG_TIMEOUT=300
