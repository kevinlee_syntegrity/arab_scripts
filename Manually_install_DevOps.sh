1,  make sure latest git, maven, openssl, wget/curl, etc.

2, install Nexus Repository Manager OSS 3.x

3, install jenkins:

Installation of a stable version
There is also a LTS YUM repository for the LTS Release Line
sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo
sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
sudo yum install jenkins

4, install docker.

https://stackoverflow.com/questions/42981114/install-docker-ce-17-03-on-rhel7
As per the documentation here, you can install Docker CE 17.03 (or future versions) on RHEL 7.3 64-bit via:

Set up the Docker CE repository on RHEL:

sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum makecache fast
Install the latest version of Docker CE on RHEL:

sudo yum -y install docker-ce
Alternatively, you can specify a specific version of Docker CE:

sudo yum -y install docker-ce-<version>-<release>
Start Docker:

sudo systemctl start docker
Test your Docker CE installation:

sudo docker run hello-world