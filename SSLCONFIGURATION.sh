#!/bin/sh
#
# Script to perform SSL Configuration for ARAB BANK
#

NODE="ABNDPR01"
SERVER="PRSRV01"

echo "=============================="
echo "SSLCONFIGURATION.sh - Started"
echo "=============================="

echo "=============================="
echo "Key Store File Configs"
echo "=============================="

mqsichangeproperties ${NODE} -e ${SERVER} -o HTTPSConnector -n keystoreFile -v /var/esb/prod/abalib/appproperties/security/IIB_keystore_prd.jks
mqsichangeproperties ${NODE} -e ${SERVER} -o HTTPSConnector -n keystorePass -v QP?hW!kY#8g
mqsichangeproperties ${NODE} -e ${SERVER} -o HTTPSConnector -n keystoreType -v JKS


echo "==============================="
echo "Trust Store File Configs"
echo "==============================="


mqsichangeproperties ${NODE} -e ${SERVER} -o ComIbmJVMManager -n truststoreFile -v /var/esb/prod/abalib/appproperties/security/truststore_prd.jks
mqsichangeproperties ${NODE} -e ${SERVER} -o ComIbmJVMManager -n truststorePass -v Hy%#tk87s?bc=
mqsichangeproperties ${NODE} -e ${SERVER} -o ComIbmJVMManager -n truststoreType -v JKS

echo "=============================="
echo "SSLCONFIGURATION.sh - Finished"
echo "=============================="
