#!/bin/sh
#
# Script to create IIB Node, Queue Manager and Execution group. It also will set HTTP and HTTPS ports, webuseradmin and enable business event for MQTT
#


#
#Assign values to the substitution variables
#
USER="iibpradm"
NODE="ABNDPR01"
QMGR="ABQMPR01"
SERVER="PRSRV01"
FUNCLVL="all"
#CACHEPORTRANGE="2880-2899"
HTTPPORT="7082"
#HTTPPORTRANGE="7800-7842"
HTTPSPORT="7843"
#HTTPSPORTRANGE="7843-7884"
MQPORT="1416"
MQTTPORT="11885"
NODEWEBPORT="4414"
RUN_FOR_REAL=${1:-NO}
DATE=`date +%Y%m%d_%H%M%S`

echo "========================================"
echo "${NODE}_Setup.sh - Started"
echo "========================================"

#
# Define the run_dryrun function and supporting code
#
UNAME=$(uname -a)
case "$UNAME" in
    *"Ubuntu"*)
        ECHO="echo"
        UNIX="Ubuntu" ;;
    *"Linux"*)
        ECHO="echo -e"
        UNIX="Linux" ;;
    *)
        ECHO="echo"
        UNIX="Other" ;;
esac

run_dryrun()
{
    if [ "$RUN_FOR_REAL" = "YES" ]
    then
        $ECHO "EXECUTING ===> $@"
        $ECHO "$@" | sh
        $ECHO
    else
       $ECHO  "DryRun ===> $@"
    fi
}

#
# Echo the variable settings to the console log
#
echo "Variable Settings"
echo "========================================"
echo "RUN FOR REAL               $RUN_FOR_REAL"
echo "Unix                       $UNIX"
echo "Echo command               $ECHO"
echo "User                       $USER"
echo "Node                       $NODE"
echo "Queue Manager              $QMGR"
echo "Server                     $SERVER"
echo "Function Level             $FUNCLVL"
#echo "Node Cache Port Range      $CACHEPORTRANGE"
#echo "Node HTTP Port Range       $HTTPPORTRANGE"
echo "Server HTTP Explicit Port  $HTTPPORT"
#echo "Node HTTPS Port Range      $HTTPSPORTRANGE"
echo "Server HTTPS Explicit Port $HTTPSPORT"
echo "MQ Port                    $MQPORT"
echo "MQTT Port                  $MQTTPORT"
echo "WebAdmin Port              $NODEWEBPORT"
echo "Date                       $DATE"

#
# Create and configure the MQ assets
#
echo "========================================"
echo "Create and start the Queue Manager"
echo "========================================"
run_dryrun crtmqm -lf 16384 -lp 5 -ls 3 -u ${QMGR}.DLQ ${QMGR}
run_dryrun sleep 5
run_dryrun strmqm ${QMGR}
run_dryrun sleep 5

echo "========================================"
echo "Define and start the TCPIP Listener Channel"
echo "========================================"
RUNMQSC="runmqsc ${QMGR} <<EOF\n
DEFINE LISTENER (LISTENER.TCP) TRPTYPE (TCP) PORT (${MQPORT}) CONTROL(QMGR)\n
START LISTENER(LISTENER.TCP)\n
END\nEOF"
run_dryrun ${RUNMQSC}

echo "========================================"
echo "Create IIB queues"
echo "========================================"
run_dryrun /opt/ibm/iib/server/sample/wmq/iib_createqueues.sh ${QMGR} mqbrkrs

echo "========================================"
echo "Define DLQ and Monitoring Pub/Sub Queue and Topic"
echo "========================================"
RUNMQSC="runmqsc ${QMGR} <<EOF\n
DEFINE QLOCAL(${QMGR}.DLQ) REPLACE\n
END\nEOF"
run_dryrun ${RUNMQSC}

echo "========================================"
echo "Create the ${NODE} Broker Node"
echo "Set the function level and cache port range"
echo "========================================"
run_dryrun mqsicreatebroker ${NODE} -q ${QMGR} -i ${USER} -a websphere -o standard -s active
run_dryrun mqsichangebroker ${NODE} -f ${FUNCLVL}
run_dryrun mqsichangebroker ${NODE} -b 'disabled'

echo "========================================"
echo "Start the ${NODE} Broker Node"
echo "========================================"
run_dryrun mqsistart ${NODE}
run_dryrun sleep 5

echo "========================================"
echo "Create EG  ${SERVER}"
echo "========================================"
run_dryrun mqsicreateexecutiongroup ${NODE} -e ${SERVER}
run_dryrun sleep 5
run_dryrun mqsilist ${NODE}

echo "========================================"
echo "JVM Settings"
echo "========================================"
run_dryrun mqsichangeproperties ${NODE} -o ComIbmJVMManager -n jvmMinHeapSize -v 100663296
run_dryrun mqsichangeproperties ${NODE} -o ComIbmJVMManager -n jvmMaxHeapSize -v 536870912
run_dryrun mqsichangeproperties ${NODE} -e ${SERVER} -o ComIbmJVMManager -n jvmMinHeapSize -v 100663296
run_dryrun mqsichangeproperties ${NODE} -e ${SERVER} -o ComIbmJVMManager -n jvmMaxHeapSize -v 536870912

echo "========================================"
echo "Add user ${USER} to webUI"
echo "========================================"
run_dryrun mqsiwebuseradmin ${NODE} -c -u ${USER} -r ${USER} -a passw0rd
run_dryrun mqsiwebuseradmin ${NODE} -l
#run_dryrun mqsichangeproperties ${NODE} -b webadmin -o HTTPConnector -n port -v ${NODEWEBPORT}
run_dryrun mqsichangeproperties ${NODE} -b webadmin -o server -n enabled,enableSSL -v true,true
run_dryrun mqsichangeproperties ${NODE} -b webadmin -o HTTPSConnector -n port,keystoreFile,keystorePass -v ${NODEWEBPORT},/var/esb/prod/abalib/appproperties/security/IIB_keystore_prd.jks,QP?hW!kY#8g


echo "========================================"
echo "Configure MQTT an enable Business Events"
echo "========================================"
run_dryrun mqsichangeproperties ${NODE} -b pubsub -o MQTTServer -n port -v ${MQTTPORT}
run_dryrun mqsichangeproperties ${NODE} -b pubsub -o MQTTServer -n enabled -v true
run_dryrun mqsichangeproperties ${NODE} -b pubsub -o BusinessEvents/MQTT -n enabled -v true

echo "========================================"
echo "Configure HTTP and HTTPS "
echo "========================================"
#run_dryrun mqsichangeproperties ${NODE} -o BrokerRegistry -n httpConnectorPortRange -v ${HTTPPORTRANGE}
#run_dryrun mqsichangeproperties ${NODE} -o BrokerRegistry -n httpsConnectorPortRange -v ${HTTPSPORTRANGE}
echo "Add explicit EG level listeners HTTP=${HTTPPORT} and HTTPS=${HTTPSPORT}"
run_dryrun mqsichangeproperties ${NODE} -e ${SERVER} -o ExecutionGroup -n httpNodesUseEmbeddedListener -v true
run_dryrun mqsichangeproperties ${NODE} -e ${SERVER} -o ExecutionGroup -n soapNodesUseEmbeddedListener -v true
run_dryrun mqsichangeproperties ${NODE} -e ${SERVER} -o HTTPConnector -n explicitlySetPortNumber -v ${HTTPPORT}
run_dryrun mqsichangeproperties ${NODE} -e ${SERVER} -o HTTPSConnector -n explicitlySetPortNumber,sslProtocol -v ${HTTPSPORT},'TLSv1.2'
echo "Disable node level listener"
run_dryrun mqsichangeproperties ${NODE} -b httplistener -o HTTPListener -n startListener -v false
run_dryrun mqsichangeproperties ${NODE} -b httplistener -o HTTPListener -n enableSSLConnector -v false

echo "========================================"
echo "${NODE}_Setup.sh - ended."
echo "========================================"
