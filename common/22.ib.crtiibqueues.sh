#!/bin/bash

echo "#"
echo "# Creating integration node queues"
echo "#"

NODEQMGR=$1
IBINSTANCEGRP=$2
IB_SYM_LINK_INSTALDIR=$3
IIBUSR=$4

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

#
# Create node queues
#

doExec "${IB_SYM_LINK_INSTALDIR}/server/sample/wmq/iib_createqueues.sh ${NODEQMGR} ${IBINSTANCEGRP}"

echo "#"
echo "# Integration node queues created successfully."
echo "#"


