#!/bin/bash

echo "#"
echo "# Check Integration Server Exist"
echo "#"

NODENAME=$1
SRV=$2
IIBUSR=$3

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

mqsistart ${NODENAME}

result=`mqsilist ${NODENAME} | grep ${SRV} | wc -l`
if [ $result -gt 0 ]; then
   echo "# ${NODENAME} - ${SRV} exist"
   exit 2
fi
   echo "# ${NODENAME} - ${SRV} is new.  Continue..."
exit 0

