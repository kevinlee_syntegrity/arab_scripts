#!/bin/bash

echo "#"
echo "# Configure MQ auths"
echo "#"

QMGRNAME=$1
ENVNAME=$2
MQGRPS=$3
SCRIPT_DIR=$4

MQUSR="mqm"

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $MQUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

echo envname = ${ENVNAME}

echo "# Enable QMgr channel auth"
doExec "echo \"ALTER QMGR CHLAUTH(ENABLED)\" | runmqsc ${QMGRNAME}"

read -a arraymqgrps <<< "$(printf "%s" "${MQGRPS}")"
TOTALGRPS=${#arraymqgrps[@]}

echo "Total groups: ${TOTALGRPS}"

for (( i=0; i<${TOTALGRPS}; i++ ))
do
	GRP=${arraymqgrps[$i]}
	RESP=`grep -q ${GRP} /etc/group`
	if [ $? -eq 0  ]; then
		echo "# Executing setmqaut for group ${GRP}. Script location: environments/${ENVNAME}/setmqaut_${GRP}.sh"
		$SCRIPT_DIR/environments/${ENVNAME}/setmqaut_${GRP}.sh ${QMGRNAME} ${GRP}	
	else
		echo "# Group ${GRP} does not exist. "
		echo "# Task: Run environments/${ENVNAME}/setmqaut_${GRP}.sh script manually"
	fi
done






