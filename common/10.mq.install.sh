#!/bin/bash

echo "#"
echo "# Install MQ instance"
echo "#"

MQTMPDIR=$1
MQSFDIR=$2
MQPKGNAME=$3
MQINSTNAME=$4
MQINSTALLDIR=$5
RPMOPT1=$6
RPMOPT2=$7
USRHOME=$8

echo "# Unzip MQ package"
mkdir -p ${MQSFDIR}/server
tar -xzf ${MQSFDIR}/${MQPKGNAME} -C ${MQSFDIR}/server

cd $MQSFDIR/server/MQServer

unset TMPDIR
export TMPDIR=${MQTMPDIR}

echo "print TMPDIR"
echo $TMPDIR

echo "# Creating mq package with instance name ${MQINSTNAME}"
./crtmqpkg ${MQINSTNAME}

echo "# Accepting QM licence"
./mqlicense.sh -accept

unset RPMDIR
RPMDIR=${MQTMPDIR}/mq_rpms/${MQINSTNAME}/x86_64
echo "# MQ package directory ${RPMDIR} "

echo "# Installing rpms"
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesRuntime*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesJRE*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesGSKit*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesClient*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesJava*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesMan*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesSample*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesSDK*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesServer*
echo "# rpms are installed."


echo "# Cleaning package directory "
rm -rf ${MQTMPDIR}/mq_rpms
rm -rf  $MQSFDIR/server

echo "#IBM MQ FASTPATH
export MQ_CONNECT_TYPE=FASTPATH" >> ${USRHOME}/mqm/.bash_profile

echo "# IBM MQ installed successfully. "


