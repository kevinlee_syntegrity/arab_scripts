#!/bin/bash

echo "#"
echo "# Configure IIB ssl"
echo "#"

NODENAME=$1
SSLDIR=$2
ENVNAME=$3
IIBUSR=$4
NODEKEYFILE=$5
NODEKEYPWD=$6
NODETRUSTFILE=$7
NODETRUSTPWD=$8
SHDIR=$9
ROOTDIR=${10}

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

KEYDIR=${SSLDIR}/${NODENAME}/keys
CERTDIR=${SSLDIR}/${NODENAME}/certs

#Copy ssl certs and keys
if [ ! -d ${CERTDIR} ];
	then
	echo "# Creating ${CERTDIR} directory to store certs"
	mkdir -p ${CERTDIR}				
fi

if [ ! -d ${KEYDIR} ];
	then
	echo "# Creating ${KEYDIR} directory to store ssl keys"		
	mkdir -p ${KEYDIR}			
fi

echo "# Copying ssl files to ${SSLDIR} directory"
if [[ "${ENVNAME}" == *"pr"* ]];
	then
	cp $ROOTDIR/ssl/prod/${NODENAME}/keys/* ${KEYDIR}	
	cp $ROOTDIR/ssl/prod/${NODENAME}/certs/* ${CERTDIR}	
else
	cp $ROOTDIR/ssl/nonprod/${NODENAME}/keys/* ${KEYDIR}		
	cp $ROOTDIR/ssl/nonprod/${NODENAME}/certs/* ${CERTDIR}		
fi

echo "# Change file owner to ${IIBUSR}"
chown -R ${IIBUSR}:mqbrkrs ${SSLDIR}

echo "# Change file permissions"
chmod -R 550 ${SSLDIR}

echo "# Enabling IIB node level SSL"
doExec "mqsichangeproperties ${NODENAME} -o BrokerRegistry -n brokerKeystoreFile -v ${KEYDIR}/${NODEKEYFILE}"
doExec "mqsichangeproperties ${NODENAME} -o BrokerRegistry -n brokerTruststoreFile -v ${CERTDIR}/${NODETRUSTFILE}"

doExec "mqsistop ${NODENAME}" 
sleep 30
doExec "mqsisetdbparms ${NODENAME} -n brokerKeystore::password -u ignore -p ${NODEKEYPWD}"
doExec "mqsisetdbparms ${NODENAME} -n brokerTruststore::password -u ignore -p ${NODETRUSTPWD}"

doExec "mqsistart ${NODENAME}"
sleep 30

doExec "mqsireportproperties ${NODENAME} -o BrokerRegistry -r"

echo "#"
echo "# ssl configuration completed"
echo "#"


