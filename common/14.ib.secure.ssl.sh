#!/bin/bash

echo "#"
echo "# Disable SSLv3, RC4, DH Ciphers Algorithms "
echo "#"

IIBDIR=$1
ALGOS="RC4 DH DHE"

JAVASECDIR=${IIBDIR}/common/jdk/jre/lib/security

for ALGO in $ALGOS
do
   result=`grep jdk.tls.disabledAlgorithms ${JAVASECDIR}/java.security | grep ${ALGO} | wc -l`
   if [ $result -eq 0 ]; then
         sed -i "/^jdk.tls.disabledAlgorithms=/ s/$/,${ALGO}/" ${JAVASECDIR}/java.security
   fi

done
echo " "
cat ${JAVASECDIR}/java.security | grep jdk.tls.disabledAlgorithms
echo " "

