#!/bin/bash

echo "#"
echo "# Configure ODBC database connections"
echo "#"

NODENAME=$1
IIBODBCDIR=$2
SHDIR=$3
ENVNAME=$4
IIBUSR=$5
CONNECTORDIR=$6
ENV=$7
DSN=$8
USERID=$9
PASS=${10}

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

echo "# Checking odbc files in  ${SHDIR}/files/odbc/${ENV} directory"
if [ ! -f ${SHDIR}/files/odbc/${ENV}/odbc.ini ] && [ ! -f ${SHDIR}/files/odbc/${ENV}/odbcinst.ini ] ;
then
	echo "# Required odbc files are not found. Please prepare odbc files specific to environment and keep in ${SHDIR}/files/odbc/${ENV} directory"	
	exit 1
else
    if [ ! -f ${IIBODBCDIR}/odbc/odbc.ini ]; then	
	echo "# Copying odbc file to ${IIBODBCDIR} directory"
	cp ${SHDIR}/files/odbc/${ENV}/odbc.ini ${SHDIR}/files/odbc/${ENV}/odbcinst.ini ${IIBODBCDIR}
	chmod -R 775 ${IIBODBCDIR}
	chown -R ${IIBUSR}:mqbrkrs ${IIBODBCDIR}
    fi 
    chmod -R 775 ${CONNECTORDIR}
    chown -R root:mqbrkrs ${CONNECTORDIR}
fi

doExec "mqsistop ${NODENAME}"
sleep 10

echo "Set ODBC credential for ${DSN}"
doExec "mqsisetdbparms ${NODENAME} -n ${DSN} -u ${USERID} -p ${PASS}"

doExec "mqsistart ${NODENAME}"
sleep 30