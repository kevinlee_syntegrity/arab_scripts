#!/bin/bash

echo "#"
echo "# Verify install packages"
echo "#"

SWDIR=$1
MQPKG=$2
IBPKG=$3

if [ ! -d $SWDIR ];
		then
			echo "Required software directory $SWDIR does not exist. Please check!"
			
			exit 1
fi
echo "# Software folder found.."
if [ ! -f $SWDIR/$MQPKG ];
		then
			echo "WMB install image $MQPKG does not exist in $SWDIR. Please check!"
			
			exit 1
fi
echo "# MQ package $MQPKG found.."
if [ ! -f $SWDIR/$IBPKG ];
		then
			echo "IIB install image $IBPKG does not exist in $SWDIR. Please check!"
			
			exit 1
fi
echo "# IIB package $IBPKG found.."

RESULT=`rpm --version`
COUNT=`echo $RESULT | grep 'RPM version'|wc -l`;
if [ $COUNT -gt 0 ]
	then
    		echo "# rpm installed"
else
    		echo "# rpm does not found to install MQ. Please install rpm and try again!"
    		exit 1
fi

RESULT=`rpmbuild`
COUNT=`echo $RESULT | grep 'command not found'|wc -l`;
if [ $COUNT -gt 0 ]
	then
    		
		echo "# rpmbuild does not found to install MQ. Please install rpmbuild and try again!"
    		exit 1
else
    		echo "# rpmbuild installed"
fi


echo "#"
echo "# Required packages/dependencies are found."
echo "#"
