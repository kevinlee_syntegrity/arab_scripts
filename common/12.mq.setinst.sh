#!/bin/bash

echo "#"
echo "# Set mq installation to primary"
echo "#"

MQINSTALLDIR=$1		# MQ Instal directory

$MQINSTALLDIR/bin/setmqinst -i -p $MQINSTALLDIR

echo "# Verify primary installation"
$MQINSTALLDIR/bin/dspmqver

$MQINSTALLDIR/bin/dspmqinst





