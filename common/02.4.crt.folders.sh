#!/bin/bash

mkdir -p /var/iib
mkdir -p /var/iib/tmp
mkdir -p /var/iib/logs
mkdir -p /var/iib/logs/old/mqstats
mkdir -p /var/iib/logs/old/resourcestats
mkdir -p /var/iib/logs/old/activity
mkdir -p /var/iib/logs/old/iib
mkdir -p /opt/oracle/jdbc/lib
chown -R iibadm:mqbrkrs /var/iib
chmod -R 775 /var/iib
chown -R iibadm:mqbrkrs /var/iib
chmod -R 775 /opt/oracle


