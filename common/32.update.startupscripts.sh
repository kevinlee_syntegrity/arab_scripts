#!/bin/bash

echo "#"
echo "# Set up startup shutdown scripts"
echo "#"

IIBUSR=$1
SHDIR=$2
NODENAME=$3
QMGR=$4

INITDDIR=/etc/rc.d/init.d

cd ${SHDIR}/files/hascripts

if [ -f "${INITDDIR}/iib-esb" ]; then
   sleep 1
else
   cp iib-esb ${INITDDIR}
   chmod 755 ${INITDDIR}/iib-esb
fi

if [[ -z "$NODENAME" ]]; then
   sleep 1
else
   result=`grep ${NODENAME} ${INITDDIR}/iib-esb | wc -l`
   if [ $result -eq 0 ]; then
      result=`grep BRKS=\"\" ${INITDDIR}/iib-esb | wc -l`
      if [ $result -eq 0 ]; then
         sed -i "/^BRKS=/ s/$/_${NODENAME}\"/" ${INITDDIR}/iib-esb
         sed -i "/^BRKS=/ s/\"_/ /" ${INITDDIR}/iib-esb
      else
         sed -i "/^BRKS=/ s/$/_${NODENAME}\"/" ${INITDDIR}/iib-esb
         sed -i "/^BRKS=/ s/\"_//" ${INITDDIR}/iib-esb
      fi
   fi

   if [[ -z "$QMGR" ]]; then
      sleep 1
   else
      result=`grep ${QMGR} ${INITDDIR}/iib-esb | wc -l`
      if [ $result -eq 0 ]; then
        result=`grep QMGRS=\"\" ${INITDDIR}/iib-esb | wc -l`
        if [ $result -eq 0 ]; then
           sed -i "/^QMGRS=/ s/$/_${QMGR}\"/" ${INITDDIR}/iib-esb
           sed -i "/^QMGRS=/ s/\"_/ /" ${INITDDIR}/iib-esb
        else
           sed -i "/^QMGRS=/ s/$/_${QMGR}\"/" ${INITDDIR}/iib-esb
           sed -i "/^QMGRS=/ s/\"_//" ${INITDDIR}/iib-esb
        fi
      fi
   fi
fi




