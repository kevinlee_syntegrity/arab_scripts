#!/bin/bash

echo "#"
echo "# Adding IIB integration Server user roles and webadmin users"
echo "#"

NODENAME=$1
NODESRV=$2
IIBSRVROLE=$3

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     $cmd
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

ROLE_NAME=${IIBSRVROLE:0:5}
SRVNO=${NODESRV:5:7}
#Confirm below role

echo "Confirm below role"
echo " Adding ${IIBSRVROLE} role with persmission to read..."
doExec "mqsichangefileauth ${NODENAME} -r ${IIBSRVROLE} -p read+"

doExec "mqsichangefileauth ${NODENAME} -r iibadmins -e ${NODESRV} -p all+"
doExec "mqsichangefileauth ${NODENAME} -r iibusers -e ${NODESRV} -p read+"
doExec "mqsichangefileauth ${NODENAME} -r ${IIBSRVROLE} -e ${NODESRV} -p all+"

echo " Adding webadmin user accounts iibadmin and iibuser..."
doExec "mqsiwebuseradmin ${NODENAME} -c -u ${ROLE_NAME}${SRVNO}user -a ${ROLE_NAME}user -r ${IIBSRVROLE}"


