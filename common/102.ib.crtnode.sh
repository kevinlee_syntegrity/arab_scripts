#!/bin/bash

echo "#"
echo "# Creating integration node"
echo "#"

NODENAME=$1
NODESHAREDPATH=$2
NODEQMGR=$3
NODEMODE=$4
NODETIMEOUT=$5
NODEEXITLILPATH=$6
IIBUSR=$7
ENVNAME=$8
SHDIR=$9

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

#
# These commands build a Integration node, server
#
if [[ "${ENVNAME}" == *"pr"* ]] || [[ "${ENVNAME}" == *"lt"* ]];
then
	if [ ! -d ${NODESHAREDPATH}/${NODENAME} ];
		then
			echo "# iib node shared workpath:${NODESHAREDPATH}/${NODENAME} does not exist. Creating one..."
			mkdir -p ${NODESHAREDPATH}/${NODENAME}
			chown -R ${IIBUSR}:mqbrkrs ${NODESHAREDPATH}
			chmod -R 755 ${NODESHAREDPATH}			
	fi
fi

ENV_LEN=${#ENVNAME}
MID=$(( ENV_LEN-2 ))
ENV_NAME=${ENV:0:$MID}
ENV_NUM=${ENV:$MID:2}
#IDX=0
IB_ENV_NAME=0
IB_ENV_NUM=${NODE:7:2}


NODELILPATH=${NODEEXITLILPATH}/${NODENAME}/lil
NODEEXTPATH=${NODEEXITLILPATH}/${NODENAME}/exit

if [ ! -d ${NODELILPATH} ];
	then
		echo "# iib lil workpath:${NODELILPATH} does not exist. Creating one..."
		mkdir -p ${NODELILPATH}
		chown -R ${IIBUSR}:mqbrkrs ${NODEEXITLILPATH}
		chmod -R 755 ${NODEEXITLILPATH}			
fi

if [ ! -d ${NODEEXTPATH} ];
	then
		echo "# iib exit workpath:${NODEEXTPATH} does not exist. Creating one..."
		mkdir -p ${NODEEXTPATH}
		chown -R ${IIBUSR}:mqbrkrs ${NODEEXITLILPATH}
		chmod -R 755 ${NODEEXITLILPATH}			
fi

if [[ "${ENVNAME}" == *"pr"* ]] || [[ "${ENVNAME}" == *"lt"* ]];
then
	echo "# Creating Integration Node  ${NODENAME}..."
	doExec "mqsicreatebroker ${NODENAME} -q ${NODEQMGR} -g ${NODETIMEOUT} -o ${NODEMODE} -e ${NODESHAREDPATH}/${NODENAME} -s active -x ${NODEEXTPATH} -l ${NODELILPATH} -b default"

else 
	echo "# Creating Integration Node  ${NODENAME}..."
	doExec "mqsicreatebroker ${NODENAME} -q ${NODEQMGR} -g ${NODETIMEOUT} -o ${NODEMODE} -s active -x ${NODEEXTPATH} -l ${NODELILPATH} -b default"

fi

echo "# Change Integration Node authmode..."
doExec "mqsichangeauthmode ${NODENAME} -s active -m file"

echo "# Change Integration Node statistics archive interval..."
doExec "mqsichangebroker ${NODENAME} -v 5"

echo "# Starting Integration Node ${NODENAME}..."
doExec "mqsistart ${NODENAME}"

sleep 5

echo "# Apply global cache policy environments/${ENVNAME}/policy_one_broker_ha.xmlfor ${NODENAME}..."
doExec "mqsichangeproperties ${NODENAME} -b cachemanager -o CacheManager -n policy -v ${SHDIR}/environments/${ENVNAME}/policy_one_broker_ha.xml"

echo "# Disabling Integration Node listener..."
doExec "mqsichangeproperties ${NODENAME} -b httplistener -o HTTPListener -n startListener -v false"

echo "#"
echo "# Integration node created successfully."
echo "#"


