#!/bin/bash

echo "#"
echo "# Check Integration Node Exist"
echo "#"

NODENAME=$1

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}


result=`mqsilist | grep ${NODENAME} | wc -l`
if [ $result -gt 0 ]; then
   echo "# ${NODENAME} exist"
   exit 2
fi
   echo "# ${NODENAME} is new.  Continue..."
exit 0

