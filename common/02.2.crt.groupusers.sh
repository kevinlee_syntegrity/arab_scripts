#!/bin/bash

echo "#"
echo "# Create groups and users"
echo "#"

MQUSR=$1
MQUID=$2
MQGRP=$3
MQGID=$4
IBUSR=$5
IBUID=$6
IBGRP=$7
IBGID=$8
UMSK=$9
USRHOME=${10}
MQSUPGRP=${11}
MQSUPGID=${12}

mkdir -p /export/home

groupadd -g $IBGID $IBGRP
echo "# $IBGRP group is created."

groupadd -g $MQGID $MQGRP
echo "# $MQGRP group is created."

groupadd -g $MQSUPGID $MQSUPGRP
echo "# $MQSUPGRP group is created."

useradd -u $IBUID -g $IBGRP -G $MQGRP -p kAEOm7CYsa5qA -d ${USRHOME}/$IBUSR $IBUSR
echo "# $IBUSR user is created."
echo "# Change user password is necessary..."

useradd -u $MQUID -g $MQGRP -p kAEOm7CYsa5qA -d ${USRHOME}/$MQUSR $MQUSR
echo "# $MQUSR user is created."
echo "# Change user password is necessary..."

echo "umask $UMSK" >> ${USRHOME}/$IBUSR/.bash_profile
echo "# umask $UMSK added to $IBUSR"
