#!/bin/bash

echo "#"
echo "# Verify install"
echo "#"

RESULT=`echo $IIB_INSTALL`

if [ "$RESULT" == "TRUE" ];
	then
		echo "IIB environment is already installed. Please clear the installation and start again!"
		exit 1
fi
echo "# IIB installation not found. Successful. 
      # Continuing..."
