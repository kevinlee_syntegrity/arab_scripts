#!/bin/bash

echo "#"
echo "# Set up startup shutdown scripts"
echo "#"

IIBUSR=$1
SHDIR=$2
USRHOME=$3

RCDIR=${USRHOME}/${IIBUSR}/scripts/rc
INITDDIR=/etc/rc.d/init.d


cd ${SHDIR}/files/hascripts

echo "Copying files to init.d"
if [ ! -f "${INITDDIR}/iib-esb" ]; then
   cp iib-esb ${INITDDIR}
   chmod 755 ${INITDDIR}/iib-esb
fi

echo "Copying files to rc directory"
mkdir -p $RCDIR	

cp rcdir/* $RCDIR
chmod -R 755 ${USRHOME}/$IIBUSR/scripts

echo "Overrite iib environment details"
sed -i "s/IIBUserName/${IIBUSR}/g" ${INITDDIR}/iib-esb

