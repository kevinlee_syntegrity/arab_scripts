#!/bin/bash

echo "#"
echo "# Verify IIB installation"
echo "#"

IBINSTALLDIR=$1

DIRNAME=`ls $IBINSTALLDIR -Art|tail -n 1` #Get the latest directory name from installation directory

cd $IBINSTALLDIR/$DIRNAME

echo "# Verify IIB checksums"
./iib verify install

echo "# Verify IIB all"
./iib verify all

echo "#"
echo "# IIB Verification complete."
echo "#"	

