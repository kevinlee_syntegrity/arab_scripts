#!/bin/bash

echo "#"
echo "# Installing integration bus component"
echo "#"

SWDIR=$1
IBINSTALLDIR=$2
IBPKGNAME=$3
IBSYMLINK=$4
IBUSR=$5
USRHOME=$6
ODBCDIR=$7
JARDIR=$8
SHDIR=$9

cd $SWDIR
mkdir -p $IBINSTALLDIR

#Redirect iib logs to /var/iib/logs
mkdir -p /var/iib/logs/old/iib
mkdir -p /var/iib/logs/old/activity
mkdir -p /var/iib/logs/old/resourcestats
mkdir -p /var/iib/logs/old/mqstats

chown -R ${IBUSR}:mqbrkrs /var/iib

tar -xzf $IBPKGNAME -C $IBINSTALLDIR
echo "# Installing integration bus component to $IBINSTALLDIR"
#tar -xzf EAsmbl_image/*.gz --exclude iib-10.0.0.[0-9]/tools -C $IBINSTALLDIR
echo "# Integration bus package is placed in $IBINSTALLDIR"

DIRNAME=`ls $IBINSTALLDIR -Art|tail -n 1`

cd $IBINSTALLDIR/$DIRNAME

echo "# Accepting license"
./iib make registry global accept license silently	

echo "# Creating symblink $IBSYMLINK to $IBINSTALLDIR/$DIRNAME"
ln -s $IBINSTALLDIR/$DIRNAME $IBSYMLINK

echo "# Adding mqsiprofie to $IBUSR"
echo "#IBM mqsipath
. $IBSYMLINK/server/bin/mqsiprofile" >> ${USRHOME}/$IBUSR/.bash_profile

echo "# Adding JAVA_HOME to $IBUSR"
echo "#IBM JAVA_HOME
export JAVA_HOME=$IBSYMLINK/common/jdk/jre/bin/java" >> ${USRHOME}/$IBUSR/.bash_profile

echo "#IBM iib working temp storage
export TMPDIR=/var/iib/tmp" >> ${USRHOME}/${IBUSR}/.bash_profile

echo "#IBM MQ FASTPATH
export MQ_CONNECT_TYPE=FASTPATH" >> ${USRHOME}/${IBUSR}/.bash_profile

echo "#IBM ODBC INI
export ODBCINI=$ODBCDIR/odbc.ini
export ODBCSYSINI=$ODBCDIR" >> ${USRHOME}/${IBUSR}/.bash_profile

echo "#IBM update rsyslog.conf"
res=`grep user.info /etc/rsyslog.conf | grep iib | wc -l`
if [ $res -le 0 ]; then
   sed -i "/local7/a\ \n#IIB Redirect to /var/iib/logs\nuser.info     /var/iib/logs/iib.log\n\n" /etc/rsyslog.conf
   service rsyslog restart
fi

mkdir -p /var/iib/tmp
chown -R ${IBUSR}:mqbrkrs /var/iib/tmp
echo "# Checking jdbc files in  ${SHDIR}/files/jdbc directory"

if [ ! -f ${SHDIR}/files/jdbc/ojdbc6.jar ];
then
        echo "# Required jdbc file are not found. Please prepare jdbc file ojdbc6.jar specific to environment and keep in ${SHDIR}/files/jdbc directory"
        
else
    if [ ! -f ${JARDIR}/ojdbc6.jar ]; then
        echo "# Copying jdbc file to ${JARDIR} directory"
        cp ${SHDIR}/files/jdbc/*.jar ${JARDIR}
        chmod -R 775 ${JARDIR}/*
    fi
fi

#echo "# Cleaning extracted files"
#rm -rf $SWDIR/EAsmbl_image

echo "#"
echo "# Integration bus component is installed in $IBINSTALLDIR/$DIRNAME"
echo "#"
