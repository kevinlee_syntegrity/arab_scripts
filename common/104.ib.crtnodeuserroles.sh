#!/bin/bash

echo "#"
echo "# Adding IIB user roles and webadmin users"
echo "#"

NODENAME=$1
NODEWEBPORT=$2
KEYFILE=$3
KEYPASS=$4
SSLDIR=$5
WEBIIBADMPWD=$6
WEBIIBUSRPWD=$7
IIBUSR=$8
NODEIP=$9

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}
ENVNAME=${NODENAME:4:2}

KEYDIR=${SSLDIR}/${NODENAME}/keys

echo " Adding :: ${IIBUSR} 'iibadmin' role with persmission to all..."
doExec "mqsichangefileauth ${NODENAME} -r iibadmins -p all+"


echo " Adding 'iibusers' role with persmission to read..."
doExec "mqsichangefileauth ${NODENAME} -r iibusers -p read+"

echo " Enabling web user interface and set port..."
doExec "mqsichangeproperties ${NODENAME} -b webadmin -o server -n enabled,enableSSL -v true,true"

# for ssl 
if [[ "$ENVNAME" == "LT" || "$ENVNAME" == "PR" ]]; then #LT and PR only
   doExec "mqsichangeproperties ${NODENAME} -b webadmin -o HTTPSConnector -n port,keystoreFile,keystorePass -v ${NODEWEBPORT},${KEYDIR}/${KEYFILE},${KEYPASS}"
else
   doExec "mqsichangeproperties ${NODENAME} -b webadmin -o HTTPSConnector -n port,keystoreFile,keystorePass,address -v ${NODEWEBPORT},${KEYDIR}/${KEYFILE},${KEYPASS},${NODEIP}"
fi

echo " Adding webadmin user accounts iibadmin and iibuser..."
doExec "mqsiwebuseradmin ${NODENAME} -c -u iibadmin -a ${WEBIIBADMPWD} -r iibadmins"
doExec "mqsiwebuseradmin ${NODENAME} -c -u iibuser -a ${WEBIIBUSRPWD} -r iibusers"
