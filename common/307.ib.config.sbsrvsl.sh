#!/bin/bash

echo "#"
echo "# Configure IIB server ssl"
echo "#"

NODENAME=$1
SRV=$2
SSLDIR=$3
ENVNAME=$4
IIBUSR=$5

doExec()
{
cmd=$1
# echo "simulating: $cmd"
echo "performing: $cmd" >&2
su - $IIBUSR -c "$cmd"
if [ $? -ne 0 ]; then
	echo "ERROR executing command: ${cmd}" >&2
	echo "Exiting script..." >&2
	exit 1
fi
}

KEYDIR=${SSLDIR}/${NODENAME}/${SRV}/keys
CERTDIR=${SSLDIR}/${NODENAME}/${SRV}/certs

if [ ! -d ${KEYDIR} ];
then
   echo "# Creating ${KEYDIR} directory to store ssl keys"
   mkdir -p ${KEYDIR}
fi

if [ ! -d ${CERTDIR} ];
then
   echo "# Creating ${CERTDIR} directory to store ssl certs"
   mkdir -p ${CERTDIR}
fi

echo "# Copying ssl files to ${KEYDIR} and ${CERTDIR} directory"
if [[ "${ENVNAME}" == *"pr"* ]];
then
   cp $ROOTDIR/ssl/prod/${NODENAME}/${SRV}/keys/* ${KEYDIR}
   cp $ROOTDIR/ssl/prod/${NODENAME}/${SRV}/certs/* ${CERTDIR}
else
   cp $ROOTDIR/ssl/nonprod/${NODENAME}/${SRV}/keys/* ${KEYDIR}
   cp $ROOTDIR/ssl/nonprod/${NODENAME}/${SRV}/certs/* ${CERTDIR}
   
fi

echo "# Change file permissions"
chmod -R 550 ${SSLDIR}
echo "# Change file owner to ${IIBUSR}"
chown -R ${IIBUSR}:mqbrkrs ${SSLDIR}

echo "#"
echo "# ssl configuration completed"
echo "#"



