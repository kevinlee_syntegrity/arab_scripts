#!/bin/bash

echo "#"
echo "# Configure IIB server ssl"
echo "#"

NODENAME=$1
SRV=$2
SSLDIR=$3
ENVNAME=$4
SRVKEYFILE=$5
SRVKEYPWD=$6
SRVTRUSTFILE=$7
SRVTRUSTPWD=$8
ROOTDIR=$9
IIBUSR=${10}

doExec()
{
cmd=$1
# echo "simulating: $cmd"
echo "performing: $cmd" >&2
su - $IIBUSR -c "$cmd"
if [ $? -ne 0 ]; then
	echo "ERROR executing command: ${cmd}" >&2
	echo "Exiting script..." >&2
	exit 1
fi
}

KEYDIR=${SSLDIR}/${NODENAME}/${SRV}/keys
CERTDIR=${SSLDIR}/${NODENAME}/${SRV}/certs

if [ ! -d ${KEYDIR} ];
then
   echo "# Creating ${KEYDIR} directory to store ssl keys"
   mkdir -p ${KEYDIR}
fi

if [ ! -d ${CERTDIR} ];
then
   echo "# Creating ${CERTDIR} directory to store ssl certs"
   mkdir -p ${CERTDIR}
fi

echo "# Copying ssl files to ${KEYDIR} and ${CERTDIR} directory"
if [[ "${ENVNAME}" == *"pr"* ]];
then
   cp $ROOTDIR/ssl/prod/${NODENAME}/${SRV}/keys/* ${KEYDIR}
   cp $ROOTDIR/ssl/prod/${NODENAME}/${SRV}/certs/* ${CERTDIR}
else
   cp $ROOTDIR/ssl/nonprod/${NODENAME}/${SRV}/keys/* ${KEYDIR}
   cp $ROOTDIR/ssl/nonprod/${NODENAME}/${SRV}/certs/* ${CERTDIR}
   
fi

echo "# Change file permissions"
chmod -R 550 ${SSLDIR}
echo "# Change file owner to ${IIBUSR}"
chown -R ${IIBUSR}:mqbrkrs ${SSLDIR}


echo "# Enabling IIB node server ${SRV} SSL"

doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o ComIbmJVMManager -n keystoreFile -v ${KEYDIR}/${SRVKEYFILE}"
doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o ComIbmJVMManager -n keystorePass -v ${SRV}Keystore::password"
doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o ComIbmJVMManager -n truststoreFile -v ${CERTDIR}/${SRVTRUSTFILE}"
doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o ComIbmJVMManager -n truststorePass -v ${SRV}Truststore::password"

doExec "mqsistop ${NODENAME}"
sleep 20

doExec "mqsisetdbparms ${NODENAME} -n ${SRV}Keystore::password -u ignore -p ${SRVKEYPWD}"

doExec "mqsisetdbparms ${NODENAME} -n ${SRV}Truststore::password -u ignore -p ${SRVTRUSTPWD}"

doExec "mqsistart ${NODENAME}"
sleep 30

doExec "mqsireportproperties ${NODENAME} -e ${SRV} -o ComIbmJVMManager -r"

echo "#"
echo "# ssl configuration completed"
echo "#"


