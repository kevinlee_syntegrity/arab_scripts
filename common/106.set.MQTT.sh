#!/bin/bash

echo "#"
echo "# Update MQTT configuration"
echo "#"

NODENAME=$1
SHAREDPATH=$2
ENVNAME=$3
MQTTPORT=$4

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     $cmd
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

if [[ "${ENVNAME}" == *"pr"* ]] || [[ "${ENVNAME}" == *"lt"* ]]; then
   sed -i '/.*bind_address.*localhost.*/c\bind_address 127.0.0.1' ${SHAREDPATH}/${NODENAME}/mqsi/components/${NODENAME}/config/${NODENAME}
else
   sed -i '/.*bind_address.*localhost.*/c\bind_address 127.0.0.1' /var/mqsi/components/${NODENAME}/config/${NODENAME}
fi

doExec "mqsichangeproperties ${NODENAME} -b pubsub -o MQTTServer -n enabled -v false"

doExec "mqsichangeproperties ${NODENAME} -b pubsub -o MQTTServer -n port -v ${MQTTPORT}"

doExec "mqsichangeproperties ${NODENAME} -b pubsub -o MQTTServer -n enabled -v true"

