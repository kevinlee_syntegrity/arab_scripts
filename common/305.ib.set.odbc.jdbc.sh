#!/bin/bash

echo "#"
echo "# Configure ODBC database connections"
echo "#"

NODENAME=$1
IIBODBCDIR=$2
SHDIR=$3
ENVNAME=$4
IIBUSR=$5
CONNECTORDIR=$6
ENV=$7
JARDIR=$8

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

echo "# Checking jdbc files in  ${SHDIR}/files/jdbc directory"
if [ ! -f ${SHDIR}/files/jdbc/ojdbc6.jar ];
then
        echo "# Required jdbc file are not found. Please prepare jdbc file ojdbc6.jar specific to environment and keep in ${SHDIR}/files/jdbc directory"
        exit 1
else
    if [ ! -f ${JARDIR}/ojdbc6.jar ]; then
        echo "# Copying jdbc file to ${JARDIR} directory"
        cp ${SHDIR}/files/jdbc/*.jar ${JARDIR}
        chmod -R 775 ${JARDIR}/*
    fi
fi

echo "# Checking odbc files in  ${SHDIR}/files/odbc/${ENV} directory"
if [ ! -f ${SHDIR}/files/odbc/${ENV}/odbc.ini ] && [ ! -f ${SHDIR}/files/odbc/${ENV}/odbcinst.ini ] ;
then
	echo "# Required odbc files are not found. Please prepare odbc files specific to environment and keep in ${SHDIR}/files/odbc/${ENV} directory"	
	exit 1
else
    if [ ! -f ${IIBODBCDIR}/odbc/odbc.ini ]; then	
	echo "# Copying odbc file to ${IIBODBCDIR} directory"
	cp ${SHDIR}/files/odbc/${ENV}/odbc.ini ${SHDIR}/files/odbc/${ENV}/odbcinst.ini ${IIBODBCDIR}
	chmod -R 775 ${IIBODBCDIR}
	chown -R ${IIBUSR}:mqbrkrs ${IIBODBCDIR}
    fi 

    chmod -R 775 ${CONNECTORDIR}
    chown -R root:mqbrkrs ${CONNECTORDIR}
fi

