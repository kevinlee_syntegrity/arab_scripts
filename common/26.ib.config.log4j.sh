#!/bin/bash

echo "#"
echo "# Configure Log4j plugin"
echo "#"

NODENAME=$1
IIBINSTALLDIR=$2
SWDIR=$3

echo "# Checking iam3.zip in ${SWDIR} directory"
if [ ! -f ${SWDIR}/iam3.zip ];
then
	echo "# iam3.zip does not found. Please check ${SWDIR} directory."	
	exit 1
else
	echo "# Unzip iam3.zip from ${SWDIR} directory"
	unzip ${SWDIR}/iam3.zip -d ${SWDIR}/iam3
		
	echo "# Copying file to IIB install directory: ${IIBINSTALLDIR}/server/jplugin"
	cp ${SWDIR}/iam3/Log4jLoggingNode_v1.2.1.jar ${IIBINSTALLDIR}/server/jplugin
	chmod -R 755 ${IIBINSTALLDIR}/server/jplugin

	echo "# Copying filess to IIB node directory: /var/mqsi/shared-classes"
	cp ${SWDIR}/iam3/Log4jLoggingNode_v1.2.1.jar ${SWDIR}/iam3/jakarta-oro-2.0.4.jar ${SWDIR}/iam3/log4j-1.2.8.jar /var/mqsi/shared-classes
	chmod -R 755 /var/mqsi/shared-classes
	
	echo "# Cleaning extracted dir.."
	rm -rf ${SWDIR}/iam3
fi


