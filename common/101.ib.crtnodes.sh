#!/bin/bash

echo "#"
echo "# Creating integration node servers"
echo "#"

NODENAMES="$@"

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2

     su - $IIBUSR -c "$cmd"
     res=$?
     if [ $res -eq 2 ]; then
          echo "# Skip ${NODE}..." # Skip an exisitng Integration Node $NODE
          skip=1 
     else
       skip=0
       if [ $res -ne 0 ]; then
          echo "ERROR executing command: ${cmd}" >&2
          echo "Exiting script..." >&2
          exit 1
       fi
     fi
}


read -a arraynodes <<< "$(printf "%s" "${NODENAMES}")"

. /tmp/iib/install/IIBv10scripts/environments/common/setenv.sh

IIBUSR=$IB_INSTANCE_ADM

TOTALNODES=${#arraynodes[@]}

for (( i=0; i<${TOTALNODES}; i++ ))
do

   NODE=${arraynodes[$i]}
   echo $NODE

   skip=0
   doExec "$SH_DIR/common/105.check.node.sh ${NODE}"
   if [ $skip -eq 0 ]; then

   IB_ENV_NAME=${NODE:4:2}
   IB_ENV_NUM=${NODE:6:2}

   case $IB_ENV_NAME in
   ET)
   ENV="eut${IB_ENV_NUM}"
   SHAREDPATH="NA"
   ENVNAME="eut"
   ;;
   ST)
   ENV="st${IB_ENV_NUM}"
   SHAREDPATH="NA"
   ENVNAME="st"
   ;;
   AT)
   ENV="at${IB_ENV_NUM}"
   SHAREDPATH="NA"
   ENVNAME="at"
   ;;
   SP)
   ENV="SP${IB_ENV_NUM}"
   SHAREDPATH="NA"
   ENVNAME="sp"
   ;;
   LT)
   ENV="lt${IB_ENV_NUM}"
   SHAREDPATH=$IB_STD_NODE_SHARED_WORKPATH
   ENVNAME="lt"
   ;;
   PR)
   ENV="pr${IB_ENV_NUM}"
   SHAREDPATH=$IB_STD_NODE_SHARED_WORKPATH
   ENVNAME="pr"
   ;;
   esac

    . $SH_DIR/environments/$ENV/setenv.sh

LISTEN2=0
PEER1=0
PEER2=0
HA1=0
HA2=0

LISTEN1=$IB_STD_NODE_GC_PORT
let LISTEN2="$IB_STD_NODE_GC_PORT + 4"
let PEER1="$IB_STD_NODE_GC_PORT + 3"
let PEER2="$IB_STD_NODE_GC_PORT + 7"
let HA1="$IB_STD_NODE_GC_PORT + 1"
let HA2="$IB_STD_NODE_GC_PORT + 5"

if [[ "$ENVNAME" == "lt" || "$ENVNAME" == "pr" ]]; then
   echo "set ha folder permission"
   chmod 2770 $IB_STD_NODE_SHARED_WORKPATH
   chown iibadm:mqbrkrs $IB_STD_NODE_SHARED_WORKPATH

   chmod 2770 $MQ_STD_NODE_WORKPATH
   chmod 2770 $MQ_STD_NODE_WORKPATH/logs
   chmod 2770 $MQ_STD_NODE_WORKPATH/qmgrs
   chown mqm:mqm $MQ_STD_NODE_WORKPATH
   chown mqm:mqm $MQ_STD_NODE_WORKPATH/logs
   chown mqm:mqm $MQ_STD_NODE_WORKPATH/qmgrs
fi

echo "# Calling script to create MQ Queue Manager..."
$SH_DIR/common/20.mq.crtmqm.sh $MQ_STD_NODE_QMGRNAME $MQ_STD_NODE_QMGRPORT $MQ_STD_NODE_QMGRLP $MQ_STD_NODE_QMGRLS $MQ_STD_NODE_QMGRLF $MQ_STD_NODE_WORKPATH $NODE_IP $ROOT_INSTALL_DIR $ENV $MQ_STD_NODE_QMGRCOM


echo "# Set MQ Queue Manager object auths..."
$SH_DIR/common/21.mq.configauth.sh $MQ_STD_NODE_QMGRNAME $ENV "$MQ_STD_GRPS" $SH_DIR #Done

echo "#Create Integration Node"
$SH_DIR/common/102.ib.crtnode.sh $NODE $SHAREDPATH $MQ_STD_NODE_QMGRNAME $IB_STD_NODE_MODE $IB_STD_NODE_CONFIG_TIMEOUT $IB_STD_NODE_USR_EXITLILPATH $IIBUSR $ENV $SH_DIR

echo "#Create Integration Node SSL"
$SH_DIR/common/103.ib.config.ssl.sh $NODE $SSL_DIR $ENV $IIBUSR $IB_STD_NODE_SSLKEYFILE $IB_STD_NODE_SSLKEYPASS $IB_STD_NODE_SSLTRUSTFILE $IB_STD_NODE_SSLTRUSTPASS $SH_DIR $ROOT_INSTALL_DIR

echo "# Create Integration Node WebAdmin Access"
$SH_DIR/common/104.ib.crtnodeuserroles.sh $NODE $IB_STD_NODE_WEB_PORT $IB_STD_NODE_SSLKEYFILE $IB_STD_NODE_SSLKEYPASS $SSL_DIR $IB_STD_WEB_IIBADM_PWD $IB_STD_WEB_IIBUSR_PWD $IIBUSR $NODE_IP

echo "# Creating IIB Node queues..." #changed
$SH_DIR/common/22.ib.crtiibqueues.sh $MQ_STD_NODE_QMGRNAME $IB_INSTANCE_GRP $IB_SYM_LINK_INSTALDIR $IB_INSTANCE_ADM #Done

echo "# Setting IIB global cache..."
$SH_DIR/common/23.ib.config.gc.sh $NODE $NODE_HOST $IB_INSTANCE_ADM $LISTEN1 $LISTEN2 $PEER1 $PEER2 $HA1 $HA2 $GLOBAL_CACHE_IP #Done

echo "# Set Log4j plugin.."
$SH_DIR/common/26.ib.config.log4j.sh $NODE $IB_SYM_LINK_INSTALDIR $SW_DIR #Done

echo "# Configure MQTT.."
doExec "$SH_DIR/common/106.set.MQTT.sh $NODE $IB_STD_NODE_SHARED_WORKPATH $ENV $IB_STD_NODE_MQTT_PORT" #Done

echo "# Set Configurable services..." #Added new
$SH_DIR/common/27.ib.configservices.sh $NODE $IB_INSTANCE_ADM $ENV $IB_STD_OLDAPOBJ $IB_STD_OLDAPAUTCONFIG $IB_STD_OLDAPAUZCONFIG $IB_STD_EDIROBJ $IB_STD_EDIRAUTCONFIG $IB_STD_EDIRAUZCONFIG $IB_STD_EMAILOBJ $IB_STD_EMAILSECID $IB_STD_EMAILSRVSTR #Done

echo "# Set Database JDBC conections.." #Added new
$SH_DIR/common/28.ib.set.jdbc.sh $NODE $SH_DIR $IIBUSR $IB_JDBC_ORA_OBJ $IB_JDBC_ORA_UID $IB_JDBC_ORA_PWD $IB_JDBC_ORA_SID $IB_JDBC_ORA_VER $IB_JDBC_ORA_DIR $IB_JDBC_ORA_MAXCONN $IB_JDBC_ORA_PORT $IB_JDBC_ORA_SRVNAME $IB_JDBC_ORA_DESC #Done

echo "# Set Database ODBC conections.." #Added new
$SH_DIR/common/29.ib.set.odbc.sh $NODE $IB_ODBC_DIR $SH_DIR $ENV $IIBUSR $IB_CONNECTOR_DIR $ENVNAME $IB_STD_ODBC_DSN $IB_STD_ODBC_UID $IB_STD_ODBC_PWD #Done

#lt change
echo "# Set Update startup scripts.." #Added new
$SH_DIR/common/32.update.startupscripts.sh $IB_INSTANCE_ADM $SH_DIR $NODE $MQ_STD_NODE_QMGRNAME #Done

echo "# Restart Node $NODE.. "
$SH_DIR/common/31.reload.ibmq.sh ${NODE} $IIBUSR

echo "# Verify $NODE.. "
$SH_DIR/common/33.verify.node.sh ${NODE} $IIBUSR

echo "---------"
echo $ENV
echo "---------"
echo $NODE
echo "---------"
echo $IIBUSR
echo "---------"
echo $NODE_IP
echo "---------"
echo $NODE_HOST
echo "---------"
echo $IB_STD_NODE_GC_PORT
echo "---------"
echo $SSL_DIR
echo "---------"
echo $SHAREDPATH
echo "---------"
echo $MQ_STD_NODE_QMGRNAME
echo "---------"
echo $IB_STD_NODE_MODE
echo "---------"
echo $IB_STD_NODE_CONFIG_TIMEOUT
echo "---------"
echo $IB_STD_NODE_USR_EXITLILPATH
echo "---------"
echo $IB_STD_NODE_WEB_PORT
echo "---------"
echo $IB_STD_NODE_SSLKEYFILE
echo "---------"
echo $IB_STD_NODE_SSLKEYPASS
echo "---------"
echo $IB_STD_WEB_IIBADM_PWD
echo "---------"
echo $IB_STD_WEB_IIBUSR_PWD
echo "---------"
echo $SH_DIR
echo "---------"

fi #node exist
done

echo "#"
echo "# Integration servers are created."
echo "#"

