#!/bin/bash

echo "#"
echo "# Check Standby Integration Node Exist"
echo "#"

NODENAME=$1
SHAREDPATH=$2

NODEDIR=${SHAREDPATH}/$NODENAME/mqsi/components/$NODENAME
echo "path $NODEDIR"

res=`mqsiaddbrokerinstance ${NODENAME} -e ${SHAREDPATH}/$NODENAME`
echo $res $?

#if [ $? -gt 0 ]; then
#   echo "# Add Node instance ${NODENAME} error."
#   exit 1
#fi
#   echo "# ${NODENAME} instance added.  Continue..."
exit 0

