#!/bin/bash

echo "#"
echo "# enable ldap group in nsswitch"
echo "#"
if [ -f /tmp/ldap.true ]; then
    sed -i '/^group:/c\group:files ldap' /etc/nsswitch.conf
    rm -f /tmp/ldap.true
fi
echo "# enable ldap group in nsswitch - completed"

