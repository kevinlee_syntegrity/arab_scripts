#!/bin/bash

echo "#"
echo "# Install MQ fixpack"
echo "#"

MQTMPDIR=$1
MQSFDIR=$2
MQFPPKGNAME=$3
MQINSTNAME=$4
MQINSTALLDIR=$5
RPMOPT1=$6
RPMOPT2=$7

MQMUSR=mqm

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     # pwd
     echo "performing: $cmd" >&2
     su $MQMUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       #echo "Exiting script..." >&2
       # exit 1
     fi
}


echo "# Unzip MQ fix pack from ${MQSFDIR}/${MQFPPKGNAME}"
mkdir -p ${MQSFDIR}/mqfixextract
tar -xzf ${MQSFDIR}/${MQFPPKGNAME} -C ${MQSFDIR}/mqfixextract

cd ${MQSFDIR}/mqfixextract

unset TMPDIR
export TMPDIR=${MQTMPDIR}

echo "# Creating mq package with instance name ${MQINSTNAME}"
./crtmqfp ${MQINSTNAME}

unset $RPMDIR
RPMDIR=${MQTMPDIR}/mq_rpms/${MQINSTNAME}/x86_64
echo "# MQ package directory ${RPMDIR} "

echo "# Installing fixpack rpms"
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesRuntime*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesJRE*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesGSKit*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesClient*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesJava*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesMan*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesSample*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesSDK*
rpm ${RPMOPT2} ${MQINSTALLDIR} ${RPMOPT1} ${RPMDIR}/MQSeriesServer*
echo "# fixpack rpms are installed."

echo "# Cleaning package directory "
rm -rf ${MQTMPDIR}/mq_rpms
rm -rf ${MQSFDIR}/mqfixextract

echo "# IBM MQ fix pack installed successfully. "
