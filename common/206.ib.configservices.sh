#!/bin/bash

echo "#"
echo "# Create Configurable services"
echo "#"

NODENAME=$1
NODESRV=$2
ACTIVITYLOGLOC=$3

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     $cmd
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

	ACTFILEPATH=${ACTIVITYLOGLOC}/${NODENAME}.${NODESRV}.activity.log
	ACTOBJ=${NODESRV}_ACTIVITY
	
	echo "# Creating Activity Log configurable service ${ACTOBJ}for server ${NODESRV}..."
	doExec "mqsicreateconfigurableservice ${NODENAME} -c ActivityLog -o ${ACTOBJ} -n filter,fileName,minSeverityLevel,formatEntries,executionGroupFilter,numberOfLogs,enabled,maxFileSizeMb,maxAgeMins -v \"RM=SAP\",\"${ACTFILEPATH}\",WARN,true,${NODESRV},1,true,0,1440"
	









