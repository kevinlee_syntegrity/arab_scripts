#!/bin/bash

echo "#"
echo "# Create Configurable services"
echo "#"

NODENAME=$1
IIBUSR=$2
ENVNAME=$3
OLDAPOBJ=$4
OLDAPAUTCONFIG=$5
OLDAPAUZCONFIG=$6
EDIROBJ=$7
EDIRAUTCONFIG=${8}
EDIRAUZCONFIG=${9}
EMAILOBJ=${10}
EMAILSECID=${11}
EMAILSRVSTR=${12}

# enable if referencing in mqsisetdbparms
#OLDAPUSR=${13}
#OLDAPPWD=${14}
#EDIRUSR=${15}
#EDIRPWD=${16}
#EMAILUSR=${17}
#EMAILPWD=${18}

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

# Open LDAP configurable service
echo "# Creating configurable service ${OLDAPOBJ} for OpenLdap..."
doExec "mqsicreateconfigurableservice ${NODENAME} -c SecurityProfiles -o ${OLDAPOBJ} -n mapping,rejectBlankpassword,propagation,passwordValue,keyStore,authorizationConfig,authenticationConfig,idToPropagateToTransport,trustStore,authentication,authorization,mappingConfig,transportPropagationConfig -v NONE,FALSE,FALSE,PLAIN,\"\",\\\"${OLDAPAUZCONFIG}\\\",\\\"${OLDAPAUTCONFIG}\\\",\"MessageID\",\"\",${OLDAPOBJ},${OLDAPOBJ},\"\",\"\""

# eDirectory configurable service
echo "# Creating configurable service ${EDIROBJ} for E-Directory..."
doExec "mqsicreateconfigurableservice ${NODENAME} -c SecurityProfiles -o ${EDIROBJ} -n mapping,rejectBlankpassword,propagation,passwordValue,keyStore,authorizationConfig,authenticationConfig,idToPropagateToTransport,trustStore,authentication,authorization,mappingConfig,transportPropagationConfig -v NONE,FALSE,FALSE,PLAIN,\"\",\\\"${EDIRAUZCONFIG}\\\",\\\"${EDIRAUTCONFIG}\\\",\"MessageID\",\"\",${EDIROBJ},${EDIROBJ},\"\",\"\""


# pop3 email configurable service
echo "# Creating Email server configurable service ${EMAILOBJ}..."
doExec "mqsicreateconfigurableservice ${NODENAME} -c EmailServer -o ${EMAILOBJ} -n serverName,securityIdentity -v \"${EMAILSRVSTR}\",${EMAILSECID}"


#Resource statistics data
echo "# Enable resourcestats on integration servers"
doExec "mqsichangeresourcestats ${NODENAME} -c active"

#Flow statistics data - nodelevel=none thread-levelnone
echo "# Enabling all flowstats on all server to minimal ..."
doExec "mqsichangeflowstats ${NODENAME} -s -g -j -c active -n none -t none -o json"

doExec "mqsistop ${NODENAME}" 
sleep 10

echo "#Set Open LDAP password - changeme"
doExec "mqsisetdbparms ${NODENAME} -n ldap::${OLDAPOBJ} -u dummyuser -p changeme"

echo "#Set E-Directory password - changeme"
doExec "mqsisetdbparms ${NODENAME} -n ldap::${EDIROBJ} -u dummyuser  -p changeme""

echo "#Set Email server password - changeme"
doExec "mqsisetdbparms ${NODENAME} -n email::${EMAILOBJ} -u dummyuser -p changeme""

doExec "mqsistart ${NODENAME}"









