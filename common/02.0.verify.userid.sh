#!/bin/bash

echo "#"
echo "# Verify user ids"
echo "#"

su - $1 -c "echo $1 login ok"

if [ $? -gt 0 ]; then
        echo "userids/groups are missing or has no login"
	echo "Creating necessary groups and users.."
  
#    ${10}/common/02.1.set.nsswitch.off.sh
   
	  ${10}/common/02.2.crt.groupusers.sh $1 $2 $3 $4 $5 $6 $7 $8 $9 ${11} ${12} ${13}
   
#    ${10}/common/02.3.set.nsswitch.on.sh
    
else
	echo "Users and groups are found"
fi


