#!/bin/bash
echo "#"
echo "# Add MQ Instance $1"
echo "#"

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

QMGR=$1
MD=$2


res=`dspmqinf $QMGR | wc -l`
if [ $res -le 3 ]; then #continue
   res=`addmqinf -v Name=${QMGR} -v Prefix=${MD} -v Directory=$QMGR`
   echo "result $?" 
   exit 0 
else
   echo "QMgr instance $QMGR exist. skipped."
   exit 0 #QMGR exists
fi
