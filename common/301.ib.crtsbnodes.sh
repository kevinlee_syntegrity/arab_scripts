#!/bin/bash

echo "#"
echo "# Creating Standby integration node servers"
echo "#"

NODENAMES="$@"

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2

     su - $IIBUSR -c "$cmd"
     res=$?
     if [ $res -eq 2 ]; then
          echo "# Skip ${NODE}..." # Skip an exisitng Integration Node $NODE
          skip=1 
     else
       skip=0
       if [ $res -ne 0 ]; then
          echo "ERROR executing command: ${cmd}" >&2
          echo "Exiting script..." >&2
          exit 1
       fi
     fi
}


read -a arraynodes <<< "$(printf "%s" "${NODENAMES}")"

. /tmp/iib/install/IIBv10scripts/environments/common/setenv.sh

IIBUSR=$IB_INSTANCE_ADM

TOTALNODES=${#arraynodes[@]}

for (( i=0; i<${TOTALNODES}; i++ ))
do

   NODE=${arraynodes[$i]}
   echo $NODE

   skip=0
   doExec "$SH_DIR/common/302.ib.check.sbnode.sh ${NODE}"
   if [ $skip -eq 0 ]; then

   IB_ENV_NAME=${NODE:4:2}
   IB_ENV_NUM=${NODE:6:2}

   case $IB_ENV_NAME in
   LT)
   ENV="lt${IB_ENV_NUM}"
   SHAREDPATH=$IB_STD_NODE_SHARED_WORKPATH
   ENVNAME="lt"
   ;;
   PR)
   ENV="pr${IB_ENV_NUM}"
   SHAREDPATH=$IB_STD_NODE_SHARED_WORKPATH
   ENVNAME="pr"
   ;;
   esac

    . $SH_DIR/environments/$ENV/setenv.sh

QMGR=IBQM${NODE:4:4}
 
chmod 2770 $IB_STD_NODE_SHARED_WORKPATH
chown iibadm:mqbrkrs $IB_STD_NODE_SHARED_WORKPATH

chmod 2770 $MQ_STD_NODE_WORKPATH/logs
chmod 2770 $MQ_STD_NODE_WORKPATH/qmgrs
chmod 2770 $MQ_STD_NODE_WORKPATH
chown mqm:mqm $MQ_STD_NODE_WORKPATH/logs
chown mqm:mqm $MQ_STD_NODE_WORKPATH/qmgrs
chown mqm:mqm $MQ_STD_NODE_WORKPATH

echo "# Calling script to add standby MQ Queue Manager..."
doExec "$SH_DIR/common/303.mq.addmqinf.sh $QMGR $MQ_STD_NODE_WORKPATH" 

echo "# Calling script to add standby IIB Node..."
doExec "$SH_DIR/common/304.ib.addsbnode.sh $NODE $IB_STD_NODE_SHARED_WORKPATH" 

echo "#Create Integration Node SSL"
$SH_DIR/common/306.ib.config.ssl.sh $NODE $SSL_DIR $ENV $IIBUSR $IB_STD_NODE_SSLKEYFILE $IB_STD_NODE_SSLKEYPASS $IB_STD_NODE_SSLTRUSTFILE $IB_STD_NODE_SSLTRUSTPASS $SH_DIR $ROOT_INSTALL_DIR

echo "# Set Log4j plugin.."
$SH_DIR/common/26.ib.config.log4j.sh $NODE $IB_SYM_LINK_INSTALDIR $SW_DIR #Done

echo "# Set Database ODBC conections.." #Added new
$SH_DIR/common/305.ib.set.odbc.jdbc.sh $NODE $IB_ODBC_DIR $SH_DIR $ENV $IIBUSR $IB_CONNECTOR_DIR $ENVNAME $IB_JDBC_ORA_DIR

#lt change
echo "# Set Update startup scripts.." #Added new
$SH_DIR/common/32.update.startupscripts.sh $IB_INSTANCE_ADM $SH_DIR $NODE $MQ_STD_NODE_QMGRNAME #Done

echo "---------"
echo $ENV
echo "---------"
echo $NODE
echo "---------"
echo $IIBUSR
echo "---------"
echo $NODE_IP
echo "---------"
echo $NODE_HOST
echo "---------"
echo $SSL_DIR
echo "---------"
echo $SHAREDPATH
echo "---------"
echo $MQ_STD_NODE_QMGRNAME
echo "---------"
echo $SH_DIR
echo "---------"

fi #node exist
done

echo "#"
echo "# Integration servers are created."
echo "#"

