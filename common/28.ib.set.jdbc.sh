#!/bin/bash

echo "#"
echo "# Configure ODBC database connections"
echo "#"

NODENAME=$1
SHDIR=$2
IIBUSR=$3
OBJ=$4
USR=$5
PASS=$6
SID=$7
VER=$8
JARDIR=$9
MAXCONN=${10}
PORT=${11}
SRVNAME=${12}
DESC=${13}

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

echo "# Checking jdbc files in  ${SHDIR}/files/jdbc directory"
if [ ! -f ${SHDIR}/files/jdbc/ojdbc6.jar ];
then
	echo "# Required jdbc file are not found. Please prepare jdbc file ojdbc6.jar specific to environment and keep in ${SHDIR}/files/jdbc directory"	
	exit 1
else
    if [ ! -f ${JARDIR}/ojdbc6.jar ]; then	
	echo "# Copying jdbc file to ${JARDIR} directory"
	cp ${SHDIR}/files/jdbc/*.jar ${JARDIR}
	chmod -R 775 ${JARDIR}/*
    fi 
fi

doExec "mqsistop ${NODENAME}"
sleep 10

echo "Set JDBC credential for ${OBJ}"
doExec "mqsisetdbparms ${NODENAME} -n jdbc::${OBJ} -u ${USR} -p ${PASS}"

doExec "mqsistart ${NODENAME}"
sleep 30

# JDBC confiugrable service


#mqsichangeproperties ${NODENAME} -c JDBCProviders -o EMPLOYEESDB -n type4DatasourceClassName,type4DriverClassName,databaseType,jdbcProviderXASupport,portNumber,serverName,connectionUrlFormatAttr1,maxConnectionPoolSize,description,jarsURL,databaseVersion,securityIdentity,connectionUrlFormat -v \"oracle.jdbc.xa.client.OracleXADataSource\",\"oracle.jdbc.OracleDriver\","Oracle","true","${PORT}","${SRVNAME}","${SID}","${MAXCONN}","${DESC}","${JARDIR}","${VER}","${OBJ}","jdbc:oracle:thin:[user]/[password]@[serverName]:[portNumber]:[connectionUrlFormatAttr1]"

doExec "mqsicreateconfigurableservice ${NODENAME} -c JDBCProviders -o ${OBJ} -n type4DatasourceClassName,type4DriverClassName,databaseType,jdbcProviderXASupport,portNumber,serverName,connectionUrlFormatAttr1,maxConnectionPoolSize,description,jarsURL,databaseVersion,securityIdentity,connectionUrlFormat -v \"oracle.jdbc.xa.client.OracleXADataSource\",\"oracle.jdbc.OracleDriver\",Oracle,true,${PORT},\"${SRVNAME}\",\"${SID}\",${MAXCONN},\"${DESC}\",${JARDIR},\"${VER}\",\"${OBJ}\",\"jdbc:oracle:thin:[user]/[password]@[serverName]:[portNumber]:[connectionUrlFormatAttr1]\""

doExec "mqsichangeproperties ${NODENAME} -c JDBCProviders -o ${OBJ} -n securityIdentity -v ${OBJ}"



