#!/bin/bash

echo "#"
echo "# Creating integration node servers"
echo "#"

NODENAME=$1
NODESRV=$2
SRVHTTPPORT=$3
SRVHTTPSPORT=$4
JVMPORT=$5
IIBUSR=$6
NODEHOST=$7
GCPORT1=$8
GCPORT2=$9
NODEIP=${10}
GCIP=${11}

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

	SRV=$NODESRV
	HTTPPORT=$SRVHTTPPORT
	HTTPSPORT=$SRVHTTPSPORT

	echo "# Creating Integration Server ${SRV}..."
	doExec "mqsicreateexecutiongroup ${NODENAME} -e ${SRV}"
		
	echo "# Starting Integration Server  ${SRV}..."
	doExec "mqsistartmsgflow ${NODENAME} -e ${SRV}"
		
	echo "# switch to using the embedded listener for a specific integration server..."
	doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o ExecutionGroup -n httpNodesUseEmbeddedListener,soapNodesUseEmbeddedListener -v true,true"
	
  #lt change
ENVNAME=${NODENAME:4:2}
if [[ "$ENVNAME" == "LT" || "$ENVNAME" == "PR" ]]; then #LT and PR only 
	echo "# Setting Integration Server  ${SRV} HTTP Port to ${HTTPPORT}..."
        doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o HTTPConnector -n explicitlySetPortNumber -v ${HTTPPORT}"
  
	echo "# Setting Integration Server  ${SRV} HTTPS Port to ${HTTPSPORT}..."
        doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o HTTPSConnector -n explicitlySetPortNumber -v ${HTTPSPORT}"
	
        echo "# Set up application Integration server ${SRV} to reference the catalog servers"
	doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o ComIbmCacheManager -n connectionEndPoints -v \\\"$GCIP:$GCPORT1,$GCIP:$GCPORT2\\\""

else  # Bind to IP
	echo "# Setting Integration Server  ${SRV} HTTP Port to ${HTTPPORT}..."
	doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o HTTPConnector -n explicitlySetPortNumber,address -v ${HTTPPORT},${NODEIP}"

	echo "# Setting Integration Server  ${SRV} HTTPS Port to ${HTTPSPORT}..."
	doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o HTTPSConnector -n explicitlySetPortNumber,address -v ${HTTPSPORT},${NODEIP}"
        
        echo "# Set up application Integration server ${SRV} to reference the catalog servers"
	doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o ComIbmCacheManager -n connectionEndPoints -v \\\"$NODEHOST:$GCPORT1,$NODEHOST:$GCPORT2\\\""

fi

	echo "# Setting JVM port to ${JVMPORT}"
	doExec "mqsichangeproperties ${NODENAME} -e ${SRV} -o ComIbmJVMManager -n jvmSystemProperty -v \"-Dcom.sun.management.jmxremote.port=${JVMPORT} -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false\""

	
echo "#"
echo "# Integration server $NODENAME created."
echo "#"

