#!/bin/bash

# QMgr creation script for MQ v8.
echo "#"
echo "# QMgr creation for MQ v8"
echo "#"

QMGRNAME=$1
QMGRPORT=$2
QMGRLP=$3
QMGRLS=$4
QMGRLF=$5
QMGRWORKPATH=$6
NODEIP=$7
ROOTDIR=$8
ENVNAME=$9
QMGRCOM="${10}"

MQMUSR="mqm" 

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     # pwd
     echo "performing: $cmd" >&2
     su $MQMUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

QMGRLD=$QMGRWORKPATH/logs
QMGRMD=$QMGRWORKPATH/qmgrs

# -----------------------------------------------------------------------#
# create QUEUE MANAGER
#
 if [ ! -d ${QMGRLD} ];
		then
			echo "mqm log path:${QMGRLD} does not exist. Creating one..."
			mkdir -p ${QMGRLD}
 fi

 if [ ! -d ${QMGRMD} ];
		then
			echo "mqm qmgr path:${QMGRMD} does not exist. Creating one..."
			mkdir -p ${QMGRMD}
 fi

chown -R mqm:mqm ${QMGRLD}
chown -R mqm:mqm ${QMGRMD}

echo "# Creating QMgr ${QMGRNAME}"
doExec "crtmqm -c ${QMGRCOM} -lc -lf ${QMGRLF} -lp ${QMGRLP} -ls ${QMGRLS} -u ${QMGRNAME}.DLQ -ld ${QMGRLD} -md ${QMGRMD} ${QMGRNAME}"

# -----------------------------------------------------------------------#
# Set queue manager to command level 801 to enable LDAP
#
  echo "# Starting QMgr ${QMGRNAME}"
  doExec "strmqm -e CMDLEVEL=801 ${QMGRNAME}"
  doExec "strmqm ${QMGRNAME}"

# -----------------------------------------------------------------------#
# Find QM directory. Get prefix and dirname from dspmqinf
#

QMGR_DIR=$QMGRMD/${QMGRNAME}
echo "# QMgr directory set to ${QMGR_DIR}"

# define and start listener
#
#lt change
ENV=${QMGRNAME:4:2}
if [[ "$ENV" == "LT" || "$ENV" == "PR" ]]; then #LT and PR only
   doExec "echo \"define listener(LISTENER.TCP) trptype(TCP) control(manual) port(${QMGRPORT})\" | runmqsc ${QMGRNAME} "
else
   doExec "echo \"define listener(LISTENER.TCP) trptype(TCP) control(manual) port(${QMGRPORT}) IPADDR(${NODEIP})\" | runmqsc ${QMGRNAME} "
fi

res=$?
if [ $res -ne 0 ] ; then
	echo "Failed to define listener on ${QMGRNAME}"
	cleanup
	exit 1
else
	doExec "echo \"start listener(LISTENER.TCP) \" | runmqsc ${QMGRNAME} "
echo
fi

# define DLQ
#
doExec "echo \"define qlocal(${QMGRNAME}.DLQ)  DESCR('${QMGRNAME} Dead Letter Queue') MAXDEPTH(100000)\" | runmqsc ${QMGRNAME} "
res=$?
if [ $res -ne 0 ] ; then
	echo "Failed to define ${QMGRNAME}.DLQ on ${QMGRNAME}"
	cleanup
fi

# -----------------------------------------------------------------------#
# Set Log Buffer Size to 8MB if it doesn't already exist
#
RES=`grep -E "LogBufferPages" ${QMGR_DIR}/qm.ini|tr -d ' '`
#echo "RES value = ${RES}"
#if [ $? -eq 0 ] ;then
	echo "Changing LogBufferPage in ${QMGR_DIR}/qm.ini"
	sed -i "s/${RES}/LogBufferPages=4096/g" ${QMGR_DIR}/qm.ini
#else
#	echo "# Task: Modify LogBufferPages to 4096 in ${QMGR_DIR}/qm.ini manually"	
#fi

# -----------------------------------------------------------------------#
# Add TCP
    linenum=`sed -n "/TCP:/=" ${QMGR_DIR}/qm.ini`
    linenum=$((linenum + 1))
    sed -i "${linenum}i\   KeepAlive=YES" ${QMGR_DIR}/qm.ini
# -----------------------------------------------------------------------#
# Add Channels
echo "CHANNELS:
	MQIBindType=FASTPATH
	MAXCHANNELS=100
	MAXACTIVECHANNELS=50
	ADOPTNEWMCA=ALL
	ADOPTNEWMCATIMEOUT=60
	ADOPTNEWMCACHECK=ALL" >> ${QMGR_DIR}/qm.ini
# -----------------------------------------------------------------------#
# Add SSL certificates
QMGR_SSL_DIR=$QMGR_DIR/ssl

doExec "mkdir -p ${QMGR_SSL_DIR}/certs"
doExec "mkdir -p ${QMGR_SSL_DIR}/keys"
doExec "chmod -R o-rx,g+w ${QMGR_SSL_DIR}/keys"
doExec "chmod -R o-rx,g+w ${QMGR_SSL_DIR}/certs"

echo "# Copying ssl files to ${SSLDIR} directory"
if [[ "${ENVNAME}" == *"pr"* ]];
	then
	doExec "cp $ROOTDIR/ssl/prod/${QMGRNAME}/keys/* ${QMGR_SSL_DIR}/keys"	
	doExec "cp $ROOTDIR/ssl/prod/${QMGRNAME}/certs/* ${QMGR_SSL_DIR}/certs"	
else
  doExec "cp $ROOTDIR/ssl/nonprod/${QMGRNAME}/keys/* ${QMGR_SSL_DIR}/keys"	
	doExec "cp $ROOTDIR/ssl/nonprod/${QMGRNAME}/certs/* ${QMGR_SSL_DIR}/certs"		
fi

# -----------------------------------------------------------------------#
#lt change
echo "Display QMGR mqs.ini info"
doExec "dspmqinf -o command ${QMGRNAME}"
# -----------------------------------------------------------------------#

