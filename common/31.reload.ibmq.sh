#!/bin/bash

echo "#"
echo "# Restarting IIB environment"
echo "#"

BRK=$1
IIBUSR=$2
TIMEOUT=60

if [[ "${BRK}" == *"all"* ]]; then
    /etc/rc.d/init.d/iib-esb stop
    sleep 15
    ps -ef|grep bip
    ps -ef|grep Data
    ps -ef|grep amq

    /etc/rc.d/init.d/iib-esb start
    ps -ef|grep bip
    ps -ef|grep Data
    ps -ef|grep amq
else
     SUFFIX=${BRK:4:4}
     QM=IBQM${SUFFIX}

     if [ ! -d /home/${IIBUSR}/scripts/rc ];
	    then
           /export/home/${IIBUSR}/scripts/rc/hamqsi_stop_broker_as $BRK $QM $IIBUSR $TIMEOUT
     else
            /home/${IIBUSR}/scripts/rc/hamqsi_stop_broker_as $BRK $QM $IIBUSR $TIMEOUT
     fi

     sleep 15
     ps -ef|grep bip
     ps -ef|grep Data
     ps -ef|grep amq


     if [ ! -d /home/${IIBUSR}/scripts/rc ];
	    then
           /export/home/${IIBUSR}/scripts/rc/hamqsi_start_broker_as $BRK $QM $IIBUSR
     else
            /home/${IIBUSR}/scripts/rc/hamqsi_start_broker_as $BRK $QM $IIBUSR
     fi
     sleep 15
     ps -ef|grep bip
     ps -ef|grep Data
     ps -ef|grep amq

fi

echo "#"
echo "# Check processes running to verify restart."
echo "#"



