#!/bin/bash
echo "#"
echo "# disable ldap group in nsswitch"
echo "#"
result=`grep group: /etc/nsswitch.conf|grep ldap|wc -l`
if [ $result -gt 0 ]; then
   sed -i '/^group:.*files.*ldap.*/c\group:files' /etc/nsswitch.conf
   touch /tmp/ldap.true
else
   echo "no change required to nsswitch.conf"
fi
echo "# disable ldap group in nsswitch - completed"
