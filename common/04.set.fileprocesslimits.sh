#!/bin/bash

echo "#"
echo "# Adding security limits to the users"
echo "#"

MQUSER=$1
IBUSER=$2

grep -E -q "mqm" /etc/security/limits.conf

if [ ! $? -eq 0 ] ; then
echo "# Set file and process limits for $MQUSER account
$MQUSER              hard    nofile          10240
$MQUSER              soft    nofile          10240
$MQUSER              hard    nproc           4096
$MQUSER              soft    nproc           4096" >> /etc/security/limits.conf

echo "# Set file and process limits for $IBUSER account
$IBUSER              hard    nofile          20480
$IBUSER              soft    nofile          20480
$IBUSER              hard    nproc           12288
$IBUSER              soft    nproc           8192" >> /etc/security/limits.conf

echo "# File and process are set."

else
	echo "# File and process are seems set. Please check manually again in /etc/security/limits.conf"
fi



