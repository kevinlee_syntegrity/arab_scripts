#!/bin/sh

#Script to create project folder structure run as root

BASE_DIR="/var/esb"
PROJECT_DIR=$1
USER_NAME=$2

mkdir -p ${BASE_DIR}/${PROJECT_DIR}

chmod u=rwx,g=rwx,o=rx ${BASE_DIR}/${PROJECT_DIR}

mkdir -p ${BASE_DIR}/${PROJECT_DIR}/logs/app

chmod u=rwx,g=rwx,o=r ${BASE_DIR}/${PROJECT_DIR}/logs/app
chmod u=rwx,g=rwx,o=r ${BASE_DIR}/${PROJECT_DIR}/logs
cd ${BASE_DIR}/${PROJECT_DIR}/logs

mkdir ${BASE_DIR}/${PROJECT_DIR}/logs/audit
chmod u=rwx,g=rwx,o=r ${BASE_DIR}/${PROJECT_DIR}/logs/audit

mkdir ${BASE_DIR}/${PROJECT_DIR}/logs/error
chmod u=rwx,g=rwx,o=r ${BASE_DIR}/${PROJECT_DIR}/logs/error

mkdir -p ${BASE_DIR}/${PROJECT_DIR}/appproperties/errormaps
chmod u=rwx,g=rwx,o=r ${BASE_DIR}/${PROJECT_DIR}/appproperties
chmod u=rwx,g=rwx,o=rx ${BASE_DIR}/${PROJECT_DIR}/appproperties/errormaps


mkdir ${BASE_DIR}/${PROJECT_DIR}/appproperties/logging
chmod u=rwx,g=rx,o=rx ${BASE_DIR}/${PROJECT_DIR}/appproperties/logging

mkdir ${BASE_DIR}/${PROJECT_DIR}/appproperties/referencedata
chmod u=rwx,g=rwx,o=rx ${BASE_DIR}/${PROJECT_DIR}/appproperties/referencedata

mkdir ${BASE_DIR}/${PROJECT_DIR}/appproperties/servicemetadata
chmod u=rwx,g=rwx,o=rx ${BASE_DIR}/${PROJECT_DIR}/appproperties/servicemetadata

chown -R ${USER_NAME}:mqbrkrs ${BASE_DIR}/${PROJECT_DIR}