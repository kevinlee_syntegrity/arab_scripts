#!/bin/bash

echo "#"
echo "# Restarting IIB Server "
echo "#"

BRK=$1
SRV=$2
IIBUSR=$3
TIMEOUT=30

doExec()
{
cmd=$1
# echo "simulating: $cmd"
echo "performing: $cmd" >&2
su - $IIBUSR -c "$cmd"
if [ $? -ne 0 ]; then
        echo "ERROR executing command: ${cmd}" >&2
        echo "Exiting script..." >&2
        exit 1
fi
}

doExec "mqsistopmsgflow ${BRK} -e ${SRV} -w ${TIMEOUT}"
sleep 5

doExec "mqsistartmsgflow ${BRK} -e ${SRV}"

echo "#"
echo "# Restart $BRK $SRV completed. "
echo "#"



