#!/bin/bash

echo "#"
echo "# Verify IIB environment"
echo "#"

NODENAME=$1
IIBUSR=$2

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}

echo "# Running mqsicvp on $NODENAME..."
doExec "mqsicvp ${NODENAME}"


echo "#"
echo "# verification completed."
echo "#"



