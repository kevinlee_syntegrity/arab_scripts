#!/bin/bash

echo "#"
echo "# Creating integration node servers"
echo "#"

NODENAME=$1
NODESRVS="$2"

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     res=$?
     if [ $res -eq 2 ]; then
         case $mode in
          SRV)
          echo "Continue..." # create an exisitng Integration Server
          skip=1
          ;;
          NODE)
          echo "Continue..."
          skip=0
          ;;
         esac
     else
       if [ $mode == "NODE" ]; then
          if [ $res -eq 0 ]; then # Node does not exist
             echo "# Node $NODENAME does not exist. Skip."
             skip=1
          else
             skip=0
             if [ $res -ne 0 ]; then
                echo "ERROR executing command: ${cmd}" >&2
                echo "Exiting script..." >&2
                exit 1
             fi
          fi
       else
             if [ $res -ne 0 ]; then
                echo "ERROR executing command: ${cmd}" >&2
                echo "Exiting script..." >&2
                exit 1
             fi
       fi
     fi
}

skip=0
. /tmp/iib/install/IIBv10scripts/environments/common/setenv.sh

IIBUSR=$IB_INSTANCE_ADM

# Check Integration Node 
mode=NODE
echo "Check Integration Node $NODENAME"
doExec "$SH_DIR/common/105.check.node.sh $NODENAME"

if [ $skip -eq 0 ]; then
   IB_ENV_NAME=${NODENAME:4:2}
   IB_ENV_NUM=${NODENAME:6:2}
   SRV_ENV_NUM=${NODESRV:6:2}

   case $IB_ENV_NAME in
     ET)
     ENV="eut${IB_ENV_NUM}"
     ;;
     ST)
     ENV="st${IB_ENV_NUM}"
     ;;
     AT)
     ENV="at${IB_ENV_NUM}"
     ;;
     SP)
     ENV="sp${IB_ENV_NUM}"
     ;;
     LT)
     ENV="lt${IB_ENV_NUM}"
     ;;
     PR)
     ENV="pr${IB_ENV_NUM}"
     ;;
   esac


   read -a arraysvrs <<< "$(printf "%s" "${NODESRVS}")"

. $SH_DIR/environments/$ENV/setenv.sh

GCPORT2=0
let GCPORT2="$IB_STD_NODE_GC_PORT + 4"
NODEIP=$NODE_IP

TOTALSRVS=${#arraysvrs[@]}

for (( i=0; i<${TOTALSRVS}; i++ ))
do
    SRV=${arraysvrs[$i]}
    echo $SRV

    SRV_ENV_NUM=${SRV:3:2}

    case $IB_ENV_NAME in
      ET)
      ROLE=iibet${SRV_ENV_NUM}users
      ;;
      SP)
      ROLE=iibsp${SRV_ENV_NUM}users
      ;;
      ST)
      ROLE=iibst${SRV_ENV_NUM}users
      ;;
      AT)
      ROLE=iibatusers
      ;;
      LT)
      ROLE=iibltusers
      ;;
      PR)
      ROLE=iibprusers
      ;;
      esac

    . $SH_DIR/environments/$ENV/$SRV.setenv.sh

    #Check Integration Server
    mode=SRV
    echo "Check Integration Server $SRV"
    doExec "$SH_DIR/common/205.ib.check.server.sh $NODENAME $SRV $IIBUSR"


    if [ $skip -eq 0 ]; then
    #Create Integration Server
    $SH_DIR/common/202.ib.crtnodesrv.sh $NODENAME $SRV $IB_SRV_HTTP_PORT $IB_SRV_HTTPS_PORT $IB_SRV_JVM_PORT $IIBUSR $NODE_HOST $IB_STD_NODE_GC_PORT $GCPORT2 ${NODEIP} $GLOBAL_CACHE_IP


    #Configure Integration Server SSL
    $SH_DIR/common/203.ib.config.ssl.sh $NODENAME $SRV $SSL_DIR $ENV $IB_SRV_SSLKEYFILE $IB_SRV_SSLKEYPASS $IB_SRV_SSLTRUSTFILE $IB_SRV_SSLTRUSTPASS $ROOT_INSTALL_DIR $IIBUSR

    echo "# Set Configurable services..." #Added new
    doExec "$SH_DIR/common/206.ib.configservices.sh $NODENAME $SRV $IB_ACTIVITY_LOGLOC" #Done

    #Create Integration Server WebAdmin Access
    doExec "$SH_DIR/common/204.ib.crtsrvuserroles.sh $NODENAME $SRV $ROLE" 

    echo "# restart IIB Node.." #Added new
    $SH_DIR/common/207.ib.reload.server.sh $NODENAME $SRV $IIBUSR #Done
    
echo $NODENAME
echo $NODEIP
echo $SRV
echo $IB_SRV_HTTP_PORT
echo $IB_SRV_HTTPS_PORT
echo $IB_SRV_JVM_PORT
echo $IIBUSR
echo $NODE_HOST
echo $IB_STD_NODE_GC_PORT
echo $GCPORT2
echo $IB_SRV_SSLKEYFILE
echo $IB_SRV_SSLKEYPASS
echo $IB_SRV_SSLTRUSTFILE
echo $IB_SRV_SSLTRUSTPASS
echo $SSL_DIR
echo $ROLE
fi # check server exists
done

fi # check node exists
echo "#"
echo "# Integration servers are created."
echo "#"
