#!/bin/bash

echo "#"
echo "# Check Standby Integration Node Exist"
echo "#"

NODENAME=$1
SHAREDPATH=$2

NODEDIR=${SHAREDPATH}/${NODENAME}/mqsi/components/${NODENAME}

if [ -d $NODEDIR ]; then
   echo "# ${NODENAME} exist. Exit."
   exit  1
fi
   echo "# ${NODENAME} missing. Continue."
exit 0

