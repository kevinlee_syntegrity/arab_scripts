#!/bin/bash

echo "#"
echo "# Configure Glodal Cache "
echo "#"

NODENAME=$1
NODEHOST=$2
IIBUSR=$3
LISTEN1=$4
LISTEN2=$5
PEER1=$6
PEER2=$7
HA1=$8
HA2=$9
GLOBALIP=${10}

LISTEN3=0
HA3=0
JMX3=0
LISTEN4=0
HA4=0
JMX4=0

let LISTEN3="$LISTEN1 + 14"
let HA3="$LISTEN3 + 1"
let JMX3="$LISTEN3 + 2"

let LISTEN4="$LISTEN1 + 17"
let HA4="$LISTEN4 + 1"
let JMX4="$LISTEN4 + 2"

doExec()
{
     cmd=$1
     # echo "simulating: $cmd"
     echo "performing: $cmd" >&2
     su - $IIBUSR -c "$cmd"
     if [ $? -ne 0 ]; then
       echo "ERROR executing command: ${cmd}" >&2
       echo "Exiting script..." >&2
       exit 1
     fi
}
if [[ ! "$GLOBALIP" == "127.0.0.1" ]]; then
    GCIP=$NODEHOST
else
    GCIP=$GLOBALIP
fi


echo "# Creating Integration Server GC0101..."
doExec "mqsicreateexecutiongroup ${NODENAME} -e GC0101"

echo "# Creating Integration Server GC0102..."
doExec "mqsicreateexecutiongroup ${NODENAME} -e GC0102"

echo "# Creating Integration Server GC0201..."
doExec "mqsicreateexecutiongroup ${NODENAME} -e GC0201"

echo "# Creating Integration Server GC0202..."
doExec "mqsicreateexecutiongroup ${NODENAME} -e GC0202"

echo "# Freeze IIB node global cache policy configuration"
doExec "mqsichangeproperties ${NODENAME} -b cachemanager -o CacheManager -n policy -v none"

echo "# Remove containers from catalog servers GC0101 and GC0102"
doExec "mqsichangeproperties ${NODENAME} -e GC0101 -o ComIbmCacheManager -n enableContainerService -v false"
doExec "mqsichangeproperties ${NODENAME} -e GC0102 -o ComIbmCacheManager -n enableContainerService -v false"

echo "# Creating Integration Server GC0203..."
doExec "mqsicreateexecutiongroup ${NODENAME} -e GC0203"

echo "# Creating Integration Server GC0204..."
doExec "mqsicreateexecutiongroup ${NODENAME} -e GC0204"

echo "# setting gc properties to GC0203..."
doExec "mqsichangeproperties ${NODENAME} -e GC0203 -o ComIbmCacheManager -n enableContainerService,enableJMX,listenerPort,listenerHost,haManagerPort,jmxServicePort,domainName -v \"true,true,${LISTEN3},${GCIP},${HA3},${JMX3},WMB_${NODENAME}_${GCIP}_${LISTEN1}_${NODENAME}_${GCIP}_${LISTEN2}\""

doExec "mqsichangeproperties ${NODENAME} -e GC0203 -o ComIbmCacheManager -n connectionEndPoints -v \\\"${GCIP}:${LISTEN1},${GCIP}:${LISTEN2}\\\""

doExec "mqsichangeproperties ${NODENAME} -e GC0203 -o ComIbmCacheManager -n catalogClusterEndPoints -v \\\"${NODENAME}_${GCIP}_${LISTEN1}:${GCIP}:${PEER1}:${HA1},${NODENAME}_${GCIP}_${LISTEN2}:${GCIP}:${PEER2}:${HA2}\\\""	

echo "# setting gc properties to GC0204..."
doExec "mqsichangeproperties ${NODENAME} -e GC0204 -o ComIbmCacheManager -n enableContainerService,enableJMX,listenerPort,listenerHost,haManagerPort,jmxServicePort,domainName -v \"true,true,${LISTEN4},${GCIP},${HA4},${JMX4},WMB_${NODENAME}_${GCIP}_${LISTEN1}_${NODENAME}_${GCIP}i_${LISTEN2}\""

doExec "mqsichangeproperties ${NODENAME} -e GC0204 -o ComIbmCacheManager -n connectionEndPoints -v \\\"${GCIP}:${LISTEN1},${GCIP}:${LISTEN2}\\\""

doExec "mqsichangeproperties ${NODENAME} -e GC0204 -o ComIbmCacheManager -n catalogClusterEndPoints -v \\\"${NODENAME}_${GCIP}_${LISTEN1}:${GCIP}:${PEER1}:${HA1},${NODENAME}_${GCIP}_${LISTEN2}:${GCIP}:${PEER2}:${HA2}\\\""	

# if Global Cache IP is 127.0.0.1
if [[ $GLOBALIP == "127.0.0.1" ]]; then

#For GC0101
echo "# changing listener host of GC0101..."
doExec "mqsichangeproperties ${NODENAME} -e GC0101 -o ComIbmCacheManager -n listenerHost -v ${GCIP}"
echo "# changing connectionEndPoints of  GC0101..."
doExec "mqsichangeproperties ${NODENAME} -e GC0101 -o ComIbmCacheManager -n connectionEndPoints -v \\\"${GCIP}:${LISTEN1},${GCIP}:${LISTEN2}\\\""
echo "# changing catalogClusterEndPoints of  GC0101..."
doExec "mqsichangeproperties ${NODENAME} -e GC0101 -o ComIbmCacheManager -n catalogClusterEndPoints -v \\\"${NODENAME}_${GCIP}_${LISTEN1}:${GCIP}:${PEER1}:${HA1},${NODENAME}_${GCIP}_${LISTEN2}:${GCIP}:${PEER2}:${HA2}\\\""
echo "# changing domainName of GC0101..."
doExec "mqsichangeproperties ${NODENAME} -e GC0101 -o ComIbmCacheManager -n domainName -v \\\"WMB_${NODENAME}_${GCIP}_${LISTEN1}_${NODENAME}_${GCIP}_${LISTEN2}\\\""

#For GC0102
echo "# changing listener host of GC0102..."
doExec "mqsichangeproperties ${NODENAME} -e GC0102 -o ComIbmCacheManager -n listenerHost -v ${GCIP}"
echo "# changing connectionEndPoints of  GC0102..."
doExec "mqsichangeproperties ${NODENAME} -e GC0102 -o ComIbmCacheManager -n connectionEndPoints -v \\\"${GCIP}:${LISTEN1},${GCIP}:${LISTEN2}\\\""
echo "# changing catalogClusterEndPoints of  GC0102..."
doExec "mqsichangeproperties ${NODENAME} -e GC0102 -o ComIbmCacheManager -n catalogClusterEndPoints -v \\\"${NODENAME}_${GCIP}_${LISTEN1}:${GCIP}:${PEER1}:${HA1},${NODENAME}_${GCIP}_${LISTEN2}:${GCIP}:${PEER2}:${HA2}\\\""
echo "# changing domainName of GC0102..."
doExec "mqsichangeproperties ${NODENAME} -e GC0102 -o ComIbmCacheManager -n domainName -v \\\"WMB_${NODENAME}_${GCIP}_${LISTEN1}_${NODENAME}_${GCIP}_${LISTEN2}\\\""

#For GC0201
echo "# changing listener host of GC0201..."
doExec "mqsichangeproperties ${NODENAME} -e GC0201 -o ComIbmCacheManager -n listenerHost -v ${GCIP}"
echo "# changing connectionEndPoints of  GC0201..."
doExec "mqsichangeproperties ${NODENAME} -e GC0201 -o ComIbmCacheManager -n connectionEndPoints -v \\\"${GCIP}:${LISTEN1},${GCIP}:${LISTEN2}\\\""
echo "# changing catalogClusterEndPoints of  GC0201..."
doExec "mqsichangeproperties ${NODENAME} -e GC0201 -o ComIbmCacheManager -n catalogClusterEndPoints -v \\\"${NODENAME}_${GCIP}_${LISTEN1}:${GCIP}:${PEER1}:${HA1},${NODENAME}_${GCIP}_${LISTEN2}:${GCIP}:${PEER2}:${HA2}\\\""
echo "# changing domainName of GC0201..."
doExec "mqsichangeproperties ${NODENAME} -e GC0201 -o ComIbmCacheManager -n domainName -v \\\"WMB_${NODENAME}_${GCIP}_${LISTEN1}_${NODENAME}_${GCIP}_${LISTEN2}\\\""

#For GC0202
echo "# changing listener host of GC0202..."
doExec "mqsichangeproperties ${NODENAME} -e GC0202 -o ComIbmCacheManager -n listenerHost -v ${GCIP}"
echo "# changing connectionEndPoints of  GC0202..."
doExec "mqsichangeproperties ${NODENAME} -e GC0202 -o ComIbmCacheManager -n connectionEndPoints -v \\\"${GCIP}:${LISTEN1},${GCIP}:${LISTEN2}\\\""
echo "# changing catalogClusterEndPoints of  GC0202..."
doExec "mqsichangeproperties ${NODENAME} -e GC0202 -o ComIbmCacheManager -n catalogClusterEndPoints -v \\\"${NODENAME}_${GCIP}_${LISTEN1}:${GCIP}:${PEER1}:${HA1},${NODENAME}_${GCIP}_${LISTEN2}:${GCIP}:${PEER2}:${HA2}\\\""
echo "# changing domainName of GC0202..."
doExec "mqsichangeproperties ${NODENAME} -e GC0202 -o ComIbmCacheManager -n domainName -v \\\"WMB_${NODENAME}_${GCIP}_${LISTEN1}_${NODENAME}_${GCIP}_${LISTEN2}\\\""

fi

doExec "mqsicacheadmin ${NODENAME} -c showPlacement"
