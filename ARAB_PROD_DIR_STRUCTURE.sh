#!/bin/sh
#
# Script to create the directory structure for Arab Bank Production Environment
#

BASEDIR="/var/esb"
PROD_USER="iibpradm"

# Create Prod Directory Structures

echo "===================================" 
echo "ARAB_PROD_DIR_STRUCTURE.sh Started"
echo "===================================="

mkdir $BASEDIR/prod
chmod u=rwx,g=rwx,o=rx $BASEDIR/prod


# Create log specific folders

mkdir -p  $BASEDIR/prod/abalib/logs

# create Audit/Message/Error log folders

cd $BASEDIR/prod/abalib/logs
mkdir -p $BASEDIR/prod/abalib/logs/app

mkdir -p $BASEDIR/prod/abalib/logs/audit

mkdir -p $BASEDIR/prod/abalib/logs/error

echo "===================================="
echo "Setting the folder permissions for logs"
echo "====================================="

find /$BASEDIR/prod/abalib/logs -type d -exec chmod 774 {} \;

echo "==================================="
echo "Create appproperty folder structure"
echo "==================================="

mkdir -p $BASEDIR/prod/abalib/appproperties/deployproperties

mkdir -p $BASEDIR/prod/abalib/appproperties/email

mkdir -p $BASEDIR/prod/abalib/appproperties/errormaps

mkdir -p $BASEDIR/prod/abalib/appproperties/jars

mkdir -p $BASEDIR/prod/abalib/appproperties/logging

mkdir -p $BASEDIR/prod/abalib/appproperties/referencedata

mkdir -p $BASEDIR/prod/abalib/appproperties/scripts

mkdir -p $BASEDIR/prod/abalib/appproperties/security

mkdir -p $BASEDIR/prod/abalib/appproperties/servicemetadata

echo "====================================="
echo "Setting file permission for appproperties"
echo "======================================"

find /$BASEDIR/prod/abalib/appproperties -type d  -exec chmod 755 {} \;

echo "======================================"
echo "Changing the owner"
echo "======================================"

sudo chown -R ${PROD_USER}:mqbrkrs /home/${}PROD_USER/${BASE_DIR}/prod




