PATH=/usr/bin:/etc:/usr/sbin:/usr/ucb:$HOME/bin:/usr/bin/X11:/sbin:.

export PATH

if [ -s "$MAIL" ]           # This is at Shell startup.  In normal
then echo "$MAILMSG"        # operation, the Shell checks
fi                          # periodically.

. /opt/ibm/mqm/inst1/bin/setmqenv -s
. /opt/ibm/iib/server/bin/mqsiprofile
mqsilist
PS1="$(whoami)@$(hostname)(\$PWD)# "
export PS1

set -o vi

umask 022

alias ll='ls -l'
#alias tailaudit='tail -f /var/esb/mystateib/logs/audit/mys.iibaudit.log'

# update node name here:
node=ABNDDV01
eg=`mqsilist $node | grep "Integration server" | head -1 | cut -d"'" -f2`
export node eg